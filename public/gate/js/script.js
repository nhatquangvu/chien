$('.buy-card').confirm({
    title: 'Xác nhận mua thẻ!',
    content: 'Bạn chắc chắn mua thẻ? Nếu trong kho còn thẻ bạn sẽ được nhận thẻ ngay lập tức,' +
        ' nếu hết thể sẽ được trả về ngay khi hệ thống có thẻ đúng mệnh giá',
});

$(document).ready( function () {
    $('.datatable').DataTable({
        "responsive" : true,
        "paging": false,
        "order": [[ 0, "desc" ]]
    });
    var t;
    window.onload = resetTimer;
    // DOM Events
    document.onmousemove = resetTimer;
    document.onkeypress = resetTimer;
    document.onload = resetTimer;
    document.onmousemove = resetTimer;
    document.onmousedown = resetTimer;
    document.ontouchstart = resetTimer;
    document.onclick = resetTimer;
    document.onscroll = resetTimer;
    document.onkeypress = resetTimer;
    function logout() {
        location.href = 'http://thenap365.com/logout';
    }

    function resetTimer() {
        clearTimeout(t);
        t = setTimeout(logout, 3000000)
    }
} );

