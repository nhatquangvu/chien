<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('password');
            $table->boolean('isShop')->default(0);
            $table->boolean('isAdmin')->default(0);
            $table->boolean("isVip")->default(0);
            $table->bigInteger('balance')->default(0);
            $table->integer('rate_gold')->default(0);
            $table->float('rate_gem',10,5)->default(0);
            $table->boolean('isBanned')->default(0);
            $table->string('bank_name')->default('');
            $table->string('bank_account')->default('');
            $table->string('bank_account_name')->default('');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
