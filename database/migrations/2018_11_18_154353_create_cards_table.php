<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cards', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('network_id');
            $table->integer('price_id');
            $table->string('serial');
            $table->string('key');
            $table->boolean('isDone')->default(0);
            $table->boolean('isCorrect')->default(1);
            $table->integer('price_in')->default(0);
            $table->integer('price_out')->default(0);
            $table->integer('member_id');
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cards');
    }
}
