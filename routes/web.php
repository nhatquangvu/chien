<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::domain('thenap365.com')->group(function () {
    Route::group(['as' => 'gate::'], function () {
        Route::any('/', ['as' => 'index', 'uses' => 'GateController@index']);
        // Route::get('/auto',['as' => 'auto', 'uses' => 'GateController@auto']);
        Route::group(['middleware' => 'checkUser'], function () {
            Route::any('/dashboard',['as' => 'dashboard', 'uses' => 'GateController@dashboard']);
            Route::any('/buy',['as' => 'buy', 'uses' => 'GateController@buy']);
            Route::any('/buy/{networkID}/{priceID}',['as' => 'buyCard', 'uses' => 'GateController@buyCard']);
            Route::any('/history',['as' => 'history', 'uses' => 'GateController@history']);
            Route::any('/user', ['as' => 'user', 'uses' => 'GateController@user']);

            Route::group(['middleware' => 'checkAdmin'],function(){
                Route::any('/error', ['as' => 'error', 'uses' => 'GateController@error']);
                Route::any('/sales', ['as' => 'sales', 'uses' => 'GateController@sales']);
                Route::any('/expired-card', ['as' => 'expired_card', 'uses' => 'GateController@expired_card']);
                Route::any('/buyer', ['as' => 'buyer', 'uses' => 'GateController@buyer']);
                Route::any('/pay', ['as' => 'pay', 'uses' => 'GateController@pay']);
                Route::any('/rate', ['as' => 'rate', 'uses' => 'GateController@rate']);
                Route::any('/user-in', ['as' => 'user_in', 'uses' => 'GateController@user_in']);
            });
            Route::get('/logout', ['as' => 'logout', 'uses' => 'GateController@logout']);
        });
    });
});
Route::domain('vangngoc24h.vn')->group(function () {
    Route::group(['as' => 'shop::'], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'ShopController@index']);
        Route::any('/login', ['as' => 'login', 'uses' => 'ShopController@login']);
        Route::any('/register', ['as' => 'register', 'uses' => 'ShopController@register']);
        Route::any('/logout', ['as' => 'logout', 'uses' => 'ShopController@logout']);

        Route::any('/gold', ['as' => 'gold', 'uses' => 'ShopController@gold']);
        Route::any('/gem', ['as' => 'gem', 'uses' => 'ShopController@gem']);
        Route::any('/card', ['as' => 'card', 'uses' => 'ShopController@card']);
        Route::any('/acc', ['as' => 'acc', 'uses' => 'ShopController@acc']);
        Route::any('/tutorial', ['as' => 'tutorial', 'uses' => 'ShopController@tutorial']);
        Route::any('/topMember', ['as' => 'top-member', 'uses' => 'ShopController@topMember']);
        Route::any('/dieu-khoan', function(){
            return view('shop.dk');
        });
        Route::get('login/facebook', 'ShopController@redirectToProvider');
        Route::get('login/facebook/callback', ['as' => 'callback_facebook','uses' => 'ShopController@handleProviderCallback']);

        Route::group(['as' => 'admin::'],function (){
            Route::any('admin/login',['as' => 'admin_login', 'uses' => 'ShopController@admin_login']);
            Route::group(['middleware'=>'checkShopAdmin'],function (){
                Route::any('admin/gem',['as' => 'admin_gem','uses' => 'ShopController@admin_gem']);
                Route::any('admin/member',['as' => 'admin_member','uses' => 'ShopController@admin_member']);
                Route::any('admin/system',['as' => 'admin_system','uses' => 'ShopController@admin_system']);
                Route::any('admin/shop',['as' => 'admin_shop','uses' => 'ShopController@admin_shop']);
                Route::any('admin/pay',['as' => 'admin_pay','uses' => 'ShopController@admin_pay']);
            });
        });

        Route::post('ajax/service',['as' => 'ajax::service','uses' => 'ShopController@ajax_service']);
        Route::post('ajax/card',['as' => 'ajax::card','uses' => 'ShopController@ajax_card']);
    });
});
Route::domain('congthe365.com')->group(function (){
//     Route::group(['as' => 'in::'], function () {

//         Route::any('/',['as' => 'index','uses' => 'InController@index']);
//         Route::any('/login',['as' => 'login','uses' => 'InController@login']);
//         Route::any('/register',['as' => 'register','uses' => 'InController@register']);
//         Route::group(['middleware' => 'checkAuthIn'],function (){
//             Route::any('/dashboard',['as' => 'dashboard','uses' => 'InController@dashboard']);
//             Route::any('/bank',['as' => 'bank','uses' => 'InController@bank']);
//             Route::any('/user',['as' => 'user','uses' => 'InController@user']);
//             Route::any('/pay',['as' => 'pay','uses' => 'InController@pay']);
//             Route::get('/logout',['as' => 'logout','uses' => 'InController@logout']);
//         });

//     });
    Route::get('login/facebook', 'ShopController@redirectToProvider');
    Route::get('login/facebook/callback', ['as' => 'callback_facebook','uses' => 'ShopController@handleProviderCallback']);
    Route::any('/dieu-khoan', function(){
        return view('shop.dk');
    });
});