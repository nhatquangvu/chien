<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    public function member(){
        return $this->hasOne('App\Member','id','user_id');
    }
}
