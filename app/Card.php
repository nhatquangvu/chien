<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    public function network(){
        return $this->hasOne('App\Network','id','network_id');
    }
    public function price(){
        return $this->hasOne('App\Price','id','price_id');
    }
    public function member(){
        return $this->hasOne('App\Member','id', 'member_id');
    }
    public function trans(){
        return $this->hasOne('App\Transaction','card_id', 'id');
    }
    public function user(){
        return $this->hasOne('App\User','id', 'user_id');
    }
}
