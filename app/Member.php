<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $fillable = [
       'name','user_id', 'fbID', 'password',
    ];
    public function user(){
        return $this->hasOne('App\User','id','user_id');
    }
}
