<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    public function member(){
        return $this->hasOne('App\Member','id','member_id');
    }
}
