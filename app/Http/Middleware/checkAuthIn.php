<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class checkAuthIn
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::check()){
            return Redirect::route('in::login')->with('notice','Bạn không có quyền truy cập');
        }
        if(Auth::user()->isShop <> 1 || Auth::user()->isVip <> 0){
            return Redirect::route('in::login')->with('notice','Bạn không có quyền truy cập');
        }
        return $next($request);
    }
}
