<?php

namespace App\Http\Controllers;
use Socialite;

use App\Account;
use App\Card;
use App\Member;
use App\Network;
use App\Page;
use App\Pay;
use App\Price;
use App\Service;
use App\Transaction;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use function PHPSTORM_META\elementType;
use Illuminate\Support\Facades\Mail;
use App\Mail\buy_gold;
class ShopController extends Controller
{
    public function index()
    {
        return view('shop.index');
    }

    public function login(Request $request)
    {
        if ($request->isMethod('get')) {
            return view('shop.login');
        } else {

            $member = Member::where('name', $request->name)->where('password', md5($request->password))->first();
            if ($member) {
                if ($member->isBanned == 0) {
                    Session::put('login_user', $member);
                    return Redirect::route('shop::index');
                } else {
                    return Redirect::route('shop::login')->with('notice', 'Tài khoản của bạn bị khóa, vui lòng liên hệ admin để hỗ trợ');
                }
            } else {
                return Redirect::route('shop::login')->with('notice', 'Sai tài khoản hoặc mật khẩu !');
            }
        }
    }

    public function register(Request $request)
    {
        if ($request->isMethod('get')) {
            return view('shop.register');
        } else {
            if ($request->password != $request->repassword) {
                return Redirect::route('shop::register')->with('notice', 'Gõ mật khậu 2 lần phải giống nhau');
            } else {
                if (Member::where('name', $request->name)->count()) {
                    return Redirect::route('shop::register')->with('notice', 'Tài khoản đã tồn tại');
                } else {
                    $member = new Member();
                    $member->name = str_slug($request->name,'-');
                    $member->password = md5($request->password);
                    $member->user_id = Session::get('user_id');
                    $member->save();
                    return Redirect::route('shop::login')->with('notice', 'Chúc mừng bạn đã đăng ký thành công. Tài khoản của bạn là : ' . $member->name );
                }
            }
        }
    }
  /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {
        $user = Socialite::driver('facebook')->fields(['name', 'first_name', 'last_name', 'email'])->user();
        $member = Member::updateOrCreate(['fbID' => $user->getId()],[
            'user_id' => 2,
            'name' => $user->name,
            'fbID' => $user->getId(),
            'password' => 'default'
        ]);
        if($member->isBanned == 0){
            Session::put('login_user', $member);
            return Redirect::to('vangngoc24h.vn');
        }else{
            return Redirect::route('shop::login')->with('notice', 'Tài khoản của bạn bị khóa, vui lòng liên hệ admin để hỗ trợ');
        }

        // $user->token;
    }
    public function logout(Request $request)
    {
        Session::forget('login_user');
        return Redirect::route('shop::index');
    }

    public function gold(Request $request)
    {
        if ($request->isMethod('get')) {
            if (Session::has('login_user')) {
                $sers = Service::where('member_id', Session::get('login_user')->id)->orderBy('id', 'desc')->where('isGold', 1)->paginate(20);
                return view('shop.gold', ['sers' => $sers]);
            } else {
                return view('shop.gold');
            }
        } else {
            $member = Member::find(Session::get('login_user')->id);
            if($request->balance < 0){
                $member->isBanned = 1;
                $member->save();
                 Session::forget('login_user');
                return Redirect::route('shop::index');
            }
            if($request->balance > 0 && $request->get('balance') <= $member->balance){
            $shop = User::find(Session::get('shop')->id);
            $serv = new Service();
            $serv->member_id = $member->id;
            $serv->isGold = 1;
            $serv->server = $request->get('server');
            $serv->account_game = $request->account_game;
            $serv->password_game = $request->password_game;
            $serv->quality = ($request->balance * $shop->rate_gold);
            $serv->balance = $request->balance;
            $serv->message = "";
            $serv->save();
            $member->balance = $member->balance - $request->balance;
            $member->save();
            Mail::to('dangchienmlnd92@gmail.com')->send(new buy_gold("Thành viên " . $member->name . "mua " . $serv->quality . "Vàng"));
                return Redirect::route('shop::gold')->with('notice', 'Mua vàng thành công');
            }else{
                return Redirect::route('shop::gold')->with('notice', 'Bạn đang cố gắng làm việc bất hợp pháp, bạn sẽ bị banned nếu tiếp tục');
            }
        }
    }

    public function gem(Request $request)
    {
        if ($request->isMethod('get')) {
            if (Session::has('login_user')) {
                $sers = Service::where('member_id', Session::get('login_user')->id)->where('isGold', 0)->orderBy('id', 'desc')->paginate(20);
                return view('shop.gem', ['sers' => $sers]);
            } else {
                return view('shop.gem');
            }
        } else {
            $member = Member::find(Session::get('login_user')->id);
             if($request->balance < 0 & ($request->balance % 50000) <> 0){
                $member->isBanned = 1;
                $member->save();
                 Session::forget('login_user');
                return Redirect::route('shop::index');
            
            }
            if($request->balance > 0 && $request->get('balance') <= $member->balance){
            $shop = User::find(Session::get('shop')->id);
            $serv = new Service();
            $serv->member_id = $member->id;
            $serv->isGold = 0;
            $serv->server = $request->get('server');
            $serv->account_game = $request->account_game;
            $serv->password_game = $request->password_game;
            $serv->quality = ($request->balance * $shop->rate_gem);
            $serv->balance = $request->balance;
            $serv->message = "";
            $serv->save();
            $member->balance = $member->balance - $request->balance;
            $member->save();
            Mail::to('dangchienmlnd92@gmail.com')->send(new buy_gold("Thành viên " . $member->name . "mua " . number_format($serv->quality) . " Ngọc"));
                return Redirect::route('shop::gem')->with('notice', 'Mua ngọc thành công');
            }else{
                return Redirect::route('shop::gem')->with('notice', 'Bạn đang cố gắng làm việc bất hợp pháp, bạn sẽ bị banned nếu tiếp tục');
            }
        }
    }

    public function card(Request $request)
    {
        if ($request->isMethod('get')) {
            if (Session::has('login_user')) {
                $cards = Card::where('member_id', Session::get('login_user')->id)->orderBy('id', 'desc')->paginate(20);
                return view('shop.card', ['cards' => $cards]);
            } else {
                return Redirect::route('shop::login');
            }
        } else {
            $user = User::find(Session::get('shop')->id);
            $network = Network::find($request->network_id);
            if ($user->isShop == 1 && $user->isVip == 1) {
                $rate_in = $network->rate_in;
            } else {
                $rate_in = $network->rate_in_guess;
            }
            if(Card::where('key',$request->get('key'))->first()){
                return Redirect::route('shop::card')->with('notice', 'Mã thẻ đã nạp');
            }
            $card = new Card();
            $card->network_id = $request->network_id;
            $card->price_id = $request->price_id;
            $card->serial = $request->serial;
            $card->key = $request->get('key');
            $card->price_in = Price::find($request->price_id)->price * ((100 - $rate_in) / 100);
            $card->member_id = Session::get('login_user')->id;
            $card->user_id = $user->id;
            $card->save();

            $transaction = Transaction::where('network_id', $request->network_id)->where('price_id', $request->price_id)->where('card_id', 0)->first();
            if ($transaction) {
                $transaction->card_id = $card->id;
                $transaction->network_id = $card->network->id;
                $transaction->price_id = $card->price->id;
                $transaction->save();
                $card->isDone = 1;
                $card->price_out = Price::find($request->price_id)->price * ((100 - $card->network->rate_out) / 100);
                $card->save();
            }
            return Redirect::route('shop::card')->with('notice', 'Hệ thống đã nhận thẻ của bạn, vui lòng đợi hệ thống xử lý. Tiền sẽ được cộng vào tài khoản khi thẻ được xác nhận thành công !');

        }
    }

    public function acc(Request $request)
    {
        if ($request->isMethod('get')) {
            if ($request->has('server')) {
                if ($request->get('numb') == NULL) {
                    $pri = explode(',', $request->get('price'));
                    $query = Account::query();
                    $query->where('price', '<=', $pri[1]);
                    $query->where('price', '>=', $pri[0]);
                    if ($request->hasPract == 1) {
                        $query->where('hasPract', 1);
                        $query->where('isNewBorn', 1);
                    }
                    if ($request->hasRing == 1) {
                        $query->where('hasRing', 1);
                    }
                    if ($request->get('server') <> 0) {
                        $query->where('server', $request->get('server'));
                    }
                    if ($request->get('planet') <> 0) {
                        $query->where('planet',$request->get('planet'));
                    }
                    $query->where('user_id', 0);
                    $account = $query->paginate(20);
                } else {
                    $account = Account::where('id', $request->get('numb'))->where('user_id', 0)->paginate(20);
                }
            }else {
                $account = Account::where('user_id', 0)->paginate(20);
            }
            if(Session::has('login_user'))
            $myacc = Account::where('user_id',Session::get('login_user')->id)->paginate(10);
            if($request->has('detail')){
                $acc_detail = Account::find($request->detail);
                return view('shop.shop',['account'=>$account,'myacc'=>$myacc, 'acc_detail'=>$acc_detail]);
            }
            if($request->has('buy')){
                $account = Account::find($request->get('buy'));
                if($account->user_id == 0){
                    $member = Member::find(Session::get('login_user')->id);
                    if($member->balance >= $account->price){
                        $member->balance -= $account->price;
                        $member->save();
                        $account->user_id = Session::get('login_user')->id;
                        $account->save();
                        Mail::to('dangchienmlnd92@gmail.com')->send(new buy_gold("Thành viên " . $member->name . "mua #" . $account->account));
                        return Redirect::route('shop::acc')->with('notice','Mua tài khoản thành công');
                    }else{
                        return Redirect::route('shop::acc')->with('notice','Không đủ tiền mua tài khoản');

                    }

                }else{
                    return Redirect::route('shop::acc')->with('notice','Tài khoản đã có người mua');
                }
            }
            if(Session::has('login_user'))
            return view('shop.shop',['account'=>$account,'myacc'=>$myacc]);
            return view('shop.shop',['account'=>$account]);
            
        }
    }

    public function tutorial(Request $request)
    {
        if ($request->isMethod('get')) {
            return view('shop.tutorial');
        }
    }

    public function topMember(Request $request){
        if($request->get('key') == 'vangngoc24h.vn'){
            foreach (Member::all() as $member){
                $member->prevM = $member->thisM;
                $member->thisM = 0;
                $member->save();
            }
        }
    }

//    Admin
    public function ajax_service(Request $request)
    {
        $id = $request->get('id');
        $service = Service::find($id);
        if ($request->has('message')) {
            $service->message = $request->get('message');
            $service->save();
        }
        return $service->message;
    }

    public function ajax_card(Request $request)
    {
        $id = $request->get('id');
        $card = Card::find($id);
        if ($card->isDone == 0) {
            return '<span class="badge badge-warning">Chờ xử lý</span>';
        } else {
            if ($card->trans) {
                if ($card->isDone == 1 && $card->trans->isUsed == 0) {
                    return '<span class="badge badge-warning">Chờ xử lý</span>';
                } elseif ($card->isDone == 1 && $card->trans->isUsed == 1 && $card->isCorrect == 1) {
                    return '<span class="badge badge-success">Thành công</span>';
                } else {
                    return '<span class="badge badge-danger">Thẻ hủy</span>';
                }
            } else {
                return '<span class="badge badge-warning">Chờ xử lý</span>';
            }
        }


    }

    public function admin_login(Request $request)
    {
        if ($request->isMethod('get')) {
            if ($request->has('logout')) {
                if (Auth::check()) {
                    Auth::logout();
                }
            }
            return view('shop.admin.login');

        } else {
            if (Auth::attempt(['name' => $request->name, 'password' => $request->password])) {
                if (Auth::user()->id == (Session::get('shop')->id)) {
                    return Redirect::route('shop::admin::admin_gem');
                } else {
                    Auth::logout();
                    return Redirect::route('shop::admin::admin_login')->with('notice', 'Bạn không có quyền truy cập');
                }
            }

        }
    }

    public function admin_gem(Request $request)
    {
        if ($request->isMethod('get')) {
            $shop = User::find(Session::get('shop')->id);
            if ($request->has('success')) {
                $ser = Service::find($request->get('success'));
                $ser->isComplete = 1;
                $ser->save();
                return Redirect::route('shop::admin::admin_gem');
            }
            if ($request->has('cancel')) {
                $ser = Service::find($request->get('cancel'));
                if ($ser->isGold == 1) {
                    $ser->member->balance += $ser->quality / $shop->rate_gold;
                } else {
                    $ser->member->balance += $ser->quality / $shop->rate_gem;
                }
                $ser->member->save();
                $ser->delete();
                return Redirect::route('shop::admin::admin_gem');

            }
            $sers = Service::orderBy('id', 'desc')->paginate(20);
            return view('shop.admin.gem', ['sers' => $sers]);
        }

    }

    public function admin_member(Request $request)
    {
        if ($request->isMethod('get')) {
            if ($request->has('banned')) {
                $mem = Member::find($request->get('banned'));
                $mem->isBanned = !$mem->isBanned;
                $mem->save();
                return Redirect::route('shop::admin::admin_member');
            }
            if($request->has('search')){
                $members = Member::where('user_id', Session::get('shop')->id)->where('name',$request->search)->paginate(30);

            }else{
                $members = Member::where('user_id', Session::get('shop')->id)->paginate(30);

            }
            return view('shop.admin.member', ['members' => $members]);
        }else{
            $member = Member::find($request->member_id);
            $member->balance += (int)$request->get('add');
            $member->save();
            return Redirect::route('shop::admin::admin_member')->with('Thêm tiền thành công');
        }
    }

    public function admin_shop(Request $request){
        if($request->isMethod('get')){
            if($request->has('add')){
                return view('shop.admin.shop_add');
            }
            if($request->has('del')){
                $account = Account::find($request->get('del'));
                $account->delete();
                return Redirect::route('shop::admin::admin_shop')->with('Xóa tài khoản thành công');
            }
            $account = Account::orderBy('id','desc')->paginate(30);
            return view('shop.admin.shop',['account'=>$account]);
        }else{
            if($request->has('add')){
                $account = new Account();
                $account->name = $request->name;
                $account->server = $request->get('server');
                $account->planet = $request->planet;
                $account->hasGmail = $request->hasGmail;
                $account->hasPract = $request->hasPract;
                $account->hasRing  = $request->hasRing;
                $account->isNewBorn  = $request->isNewBorn;
                $account->price = $request->price;
                $account->account = $request->account;
                $account->password = $request->password;
                $account->content = $request->get('content');
                $photoName = time().'.'.Input::file('image')->getClientOriginalExtension();
                Input::file('image')->move(public_path('shop/account'), $photoName);
                $account->image = $photoName;
                $account->save();
                return Redirect::route('shop::admin::admin_shop')->with('Thêm tài khoản thành công');
            }else{
                $account = Account::find($request->get('acc_id'));
                $account->price= $request->get('price');
                $account->save();
                return Redirect::route('shop::admin::admin_shop')->with('SỬA tài khoản thành công');

            }
        }
    }


    public function admin_system(Request $request)
    {
        if ($request->isMethod('get')) {
            return view('shop.admin.system');
        } else {
            $shop = User::find(Session::get('shop')->id);
            $shop->rate_gold = $request->rate_gold;
            $shop->rate_gem = $request->rate_gem;
            $shop->save();
            $page = Page::where('short_code', 'shop_1')->first();
            $page->content = $request->warning;
            $page->save();
            $page = Page::where('short_code', 'shop_1_hotline')->first();
            $page->content = $request->hotline;
            $page = Page::where('short_code', 'shop_1_popup')->first();
            $page->content = $request->get('popup');
            $page->save();
            $page = Page::where('short_code', 'shop_1_atm')->first();
            $page->content = $request->get('atm');
            $page->save();
            $page = Page::where('short_code', 'shop_1_top')->first();
            $page->content = $request->get('top');
            $page->save();
            return Redirect::route('shop::admin::admin_system');
        }
    }
    public function admin_pay(Request $request){
        if($request->isMethod('get')){
            $pays = Pay::where('user_id',Auth::user()->id)->orderBy('id','desc')->get();
            return view('shop.admin.pay',['pays' => $pays]);
        }else{
            if(Auth::user()->balance >= $request->balance){
                Auth::user()->balance -= $request->balance;
                Auth::user()->save();
                $pay = new Pay();
                $pay->user_id = Auth::user()->id;
                $pay->balance = $request->balance;
                $pay->isIn =0;
                $pay->status = 0;
                $pay->save();
                return Redirect::route('shop::admin::admin_pay')->with('notice','Yêu cầu rút tiền thành công');
            }else{
                return Redirect::route('shop::admin::admin_pay')->with('notice','Không đủ tiền');

            }

        }
    }
}