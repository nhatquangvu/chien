<?php

namespace App\Http\Controllers;

use App\Card;
use App\Network;
use App\Pay;
use App\Price;
use App\Transaction;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class InController extends Controller
{
    public function index(Request $request){
        return view('in.index');
    }
    public function login(Request $request)
    {
        if ($request->isMethod('get')) {
            return view('in.login');
        } else {

            if (Auth::attempt(['name' => $request->name, 'password' => $request->password])) {
                if(Auth::user()->isBanned == 1){
                    Auth::logout();
                    return Redirect::route('in::dashboard')->with('notice','Tài khoản bị vô hiệu hóa, xin vui lòng liên hệ admin');
                }
                return Redirect::route('in::dashboard');
            } else {
                return Redirect::route('in::login')->with('notice', 'Sai mật khẩu hoặc tài khoản');
            }
        }
    }

    public function register(Request $request)
    {
        if ($request->isMethod('get')) {
            return view('in.register');
        } else {
            if ($request->password <> $request->repassword) {
                return Redirect::route('in::register')->with('notice', 'Gõ lại mật khẩu không đúng');
            } else {
                $user = User::where('name', $request->name)->first();
                if (!$user) {
                    $user = new User();
                    $user->name = $request->name;
                    $user->password = bcrypt($request->password);
                    $user->isShop = 1;
                    $user->save();
                    return Redirect::route('in::login')->with('notice', 'Đăng ký tài khoản thành công');
                } else {
                    return Redirect::route('in::register')->with('notice', 'Tài khoản đã tồn tại');
                }
            }
        }
    }

    public function dashboard(Request $request)
    {
        if ($request->isMethod('get')) {
            $cards = Card::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->paginate(30);
            return view('in.dashboard', ['cards' => $cards]);
        } else {
            $user = Auth::user();
            $network = Network::find($request->network_id);
            if ($user->isShop == 1 && $user->isVip == 1)
                $rate_in = $network->rate_in;
            else
                $rate_in = $network->rate_in_guess;
            $card = new Card();
            $card->network_id = $request->network_id;
            $card->price_id = $request->price_id;
            $card->serial = $request->serial;
            $card->key = $request->get('key');
            $card->price_in = Price::find($request->price_id)->price * ((100 - $rate_in) / 100);
            $card->member_id = 0;
            $card->user_id = $user->id;
            $card->save();

            $transaction = Transaction::where('network_id', $request->network_id)->where('price_id', $request->price_id)->where('card_id', 0)->first();
            if ($transaction) {
                $transaction->card_id = $card->id;
                $transaction->network_id = $card->network->id;
                $transaction->price_id = $card->price->id;
                $transaction->save();
                $card->isDone = 1;
                $card->price_out = Price::find($request->price_id)->price * ((100 - $card->network->rate_out) / 100);
                $card->save();
            }
            return Redirect::route('in::dashboard')->with('notice', 'Nạp thẻ thành công');
        }
    }

    public function bank(Request $request)
    {
        if($request->isMethod('get')){
            return view('in.bank');
        }else{
            $user = Auth::user();
            $user->bank_name = $request->bank_name;
            $user->bank_account = $request->bank_account;
            $user->bank_account_name = $request->bank_account_name;
            $user->save();
            return Redirect::route('in::bank')->with('notice','Thông tin ngân hàng cập nhập thành công');
        }
    }
    public function user(Request $request){
        if($request->isMethod('get')){
            return view('in.user');
        }else{
            if($request->password <> $request->repassword){
                return Redirect::route('in::user')->with('notice','Gõ lại mật khẩu không đúng');
            }else{
                $user = Auth::user();
                $user->password = bcrypt($request->password);
                $user->save();
                Auth::logout();
                return Redirect::route('in::login')->with('notice','Thay đổi mật khẩu thành công, vui lòng đăng nhập lại');
            }

        }
    }
    public function pay(Request $request){
        if($request->isMethod('get')){
            $pays = Pay::where('user_id',Auth::user()->id)->orderBy('id','desc')->get();
            return view('in.pay',['pays' => $pays]);
        }else{
            if(Auth::user()->balance >= $request->balance){
                Auth::user()->balance -= $request->balance;
                Auth::user()->save();
                $pay = new Pay();
                $pay->user_id = Auth::user()->id;
                $pay->balance = $request->balance;
                $pay->isIn =0;
                $pay->status = 0;
                $pay->save();
                return Redirect::route('in::pay')->with('notice','Yêu cầu rút tiền thành công');
            }else{
                return Redirect::route('in::pay')->with('notice','Tiền trong tài khoản không đủ');

            }

        }
    }
    public function logout()
    {
        Auth::logout();
        return Redirect::route('in::login');
    }

}
