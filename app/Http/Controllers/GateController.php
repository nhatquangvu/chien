<?php

namespace App\Http\Controllers;

use App\Card;
use App\Network;
use App\Pay;
use App\Price;
use App\Transaction;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class GateController extends Controller
{
    public function index(Request $request)
    {
        if ($request->isMethod('get')) {
            if (!Auth::check()) {
                return view('gate.index');
            } else {
                return Redirect::route('gate::dashboard');
            }
        } else {
            if (Auth::attempt(['name' => $request->username, 'password' => $request->password])) {
                if (Auth::user()->isBanned == 1) {
                    Auth::logout();
                    return Redirect::route('gate::index')->with('notice', 'Tài khoản bị khóa');
                }
                return Redirect::route('gate::dashboard');
            } else {
                return Redirect::route('gate::index')->with('notice', 'Mật khẩu hoặc tên tài khoản không đúng');
            }
        }
    }

    public function dashboard(Request $request)
    {
        if ($request->isMethod('get')) {
            return view('gate.dashboard');
        }
    }

    public function buy(Request $request)
    {
        if ($request->isMethod('get')) {
            $networks = Network::all();
            $prices = Price::all();
            return view('gate.buy', ['networks' => $networks, 'prices' => $prices]);
        }
    }

    public function buyCard(Request $request)
    {
        $network = Network::find($request->networkID);
        $price = Price::find($request->priceID);
        $user = Auth::user();
        $priceOut = ($price->price / 100) * (100 - $network->rate_out);
        if ($user->balance < $priceOut) {
            return Redirect::route('gate::buy')->with('notice', 'Bạn không đủ tiền để thực hiện giao dịch');
        } elseif (Transaction::where('user_id', $user->id)->where('card_id', 0)->count() == 5) {
            return Redirect::route('gate::history')->with('notice', 'Bạn chỉ được phép thực hiện 5 giao dịch chờ liên tiếp .');
        } else {
            $transaction = new Transaction();
            $card = Card::where('price_id', $price->id)
                ->where('network_id', $network->id)
                ->where('isDone', 0)
                ->first();
            if ($card) {
                $transaction->card_id = $card->id;
                $transaction->network_id = $network->id;
                $transaction->price_id = $price->id;
                $transaction->user_id = $user->id;
                $transaction->save();
                $card->isDone = 1;
                $card->price_out = $priceOut;
                $card->save();
            } else {
                $transaction->card_id = 0;
                $transaction->network_id = $network->id;
                $transaction->price_id = $price->id;
                $transaction->user_id = $user->id;
                $transaction->save();
            }


            $user->balance = $user->balance - $priceOut;
            $user->save();
            if ($card) {
                return Redirect::route('gate::history', ['isDone' => 1]);
            } else {
                return Redirect::route('gate::history')->with('notice', 'Thêm một giao dịch chờ thành công. Vui lòng đợi hệ thống cấp thẻ');
            }
        }
    }

    public function history(Request $request)
    {
        if ($request->isMethod('get')) {
            if (!$request->has('isDone')) {
                $transaction = Transaction::where('user_id', Auth::user()->id)
                    ->orderBy('created_at', 'desc')
                    ->paginate(30);
            } else {
                $transaction = Transaction::where('user_id', Auth::user()->id)
                    ->where('card_id', "<>", 0)
                    ->orderBy('created_at', 'desc')
                    ->paginate(30);
            }
            if ($request->has('check')) {
                $trans = Transaction::find($request->get('check'));
                if ($trans && $trans->isUsed == 0) {
                    $trans->isUsed = 1;
                    $trans->save();
                    $trans->card->isCorrect = 1;
                    if ($trans->card->user->isVip == 0)
                        $trans->card->user->balance += ($trans->card->price->price * ((100 - $trans->card->network->rate_in_guess) / 100));
                    else
                        $trans->card->user->balance += ($trans->card->price->price * ((100 - $trans->card->network->rate_in) / 100));
                    $trans->card->user->save();
                    if ($trans->card->member_id <> 0) {
                        $trans->card->member->balance += $trans->card->price->price;
                        $trans->card->member->thisM += $trans->card->price->price;
                        $trans->card->member->save();

                    }
                    $trans->card->save();
                }
                return Redirect::route('gate::history');
            }
            if ($request->has('recheck')) {
                $trans = Transaction::find($request->get('recheck'));
                if ($trans && $trans->card->isCorrect == 0) {
                    $trans->isUsed = 1;
                    $trans->save();
                    $trans->card->isCorrect = 1;
                    if ($trans->card->user->isVip == 0)
                        $trans->card->user->balance += ($trans->card->price->price * ((100 - $trans->card->network->rate_in_guess) / 100));
                    else
                        $trans->card->user->balance += ($trans->card->price->price * ((100 - $trans->card->network->rate_in) / 100));
                    $trans->card->user->save();
                    if ($trans->card->member_id <> 0) {
                        $trans->card->member->balance += $trans->card->price->price;
                        $trans->card->member->thisM += $trans->card->price->price;
                        $trans->card->member->save();
                    }
                    $trans->card->save();
                    $trans->user->balance -= ($trans->card->price->price * ((100 - $trans->card->network->rate_out) / 100));
                    $trans->user->save();
                }
                return Redirect::route('gate::history');
            }
            if ($request->has('error')) {
                $trans = Transaction::find($request->get('error'));
                if ($trans && $trans->isUsed == 0) {
                    Auth::user()->balance += ($trans->card->price->price * ((100 - $trans->card->network->rate_out) / 100));
                    Auth::user()->save();
                    $trans->isUsed = 1;
                    $trans->card->isCorrect = 0;
                    $trans->card->price_in = 0;
                    $trans->card->price_out = 0;
                    $trans->card->save();
                    $trans->save();
                }
                return Redirect::route('gate::history');
            }
            if ($request->has('del')) {
                $tran = Transaction::find($request->get('del'));
                if ($tran && $tran->card_id == '') {
                    Auth::user()->balance += ($tran->price->price * ((100 - $tran->network->rate_out) / 100));
                    Auth::user()->save();
                    $tran->delete();
                }
                return Redirect::route('gate::history');

            }
            return view('gate.history', ['transaction' => $transaction]);


        }
    }

    public function user(Request $request)
    {
        if ($request->isMethod('get')) {
            return view('gate.user');
        } else {
            if ($request->password == $request->repassword) {
                Auth::user()->password = bcrypt($request->password);
                Auth::user()->save();
                Auth::logout();
                return Redirect::route('gate::index')->with('notice', 'Thay đổi mật khẩu thành công');

            } else {
                return Redirect::back()->with('notice', 'Gõ mật khẩu không trùng khớp');
            }
        }
    }

    public function expired_card(Request $request)
    {
        if ($request->isMethod('get')) {
            if ($request->has('back')) {

                $card = Card::find($request->get('back'));
                if ($card->isDone == 0) {
                    $card->isDone = 1;
                    $card->price_out = $card->price->price * ((100 - Auth::user()->rate_in) / 100);
                    $card->save();
                }
            }
            if ($request->has('error')) {
                $card = Card::find($request->get('error'));
                if ($card->isDone == 0) {
                    $card->isDone = 1;
                    $card->price_out = 0;
                    $card->price_in = 0;
                    $card->save();
                }
            }
            $expired_cards = Card::where('isDone', 0)->where('created_at', '<',
                Carbon::now()->subHours(20))->paginate(30);
            return view('gate.expired_card', ['expired_cards' => $expired_cards]);
        }
    }

    public function buyer(Request $request)
    {
        if ($request->isMethod('get')) {
            if ($request->has('vip')) {
                $user = User::find($request->get('vip'));
                $user->isVip = !$user->isVip;
                $user->save();
                return Redirect::route('gate::buyer');
            }
            if ($request->has('banned')) {
                $user = User::find($request->get('banned'));
                $user->isBanned = !$user->isBanned;
                $user->save();
                return Redirect::route('gate::buyer');
            }
            $users = User::where('isShop', 0)->where('isAdmin', 0)->get();
            $pays = Pay::paginate(30);
            return view('gate.buyer', ['users' => $users, 'pays' => $pays]);
        } else {
            if ($request->has('addUser')) {
                if (User::where('name', $request->name)->count() <> 0) {
                    return Redirect::route('gate::buyer')->with('notice', 'Tên người dùng đã có');
                }
                $user = new User();
                $user->name = $request->name;
                $user->password = bcrypt($request->password);
                $user->isVip = $request->isVip;
                $user->save();
                return Redirect::route('gate::buyer')->with('notice', 'Thêm đại lý thành công');
            } elseif ($request->has('removeBalance')) {
                $user = User::find($request->user_id);
                $user->balance -= (int)($request->balance);
                $user->save();
                $pay = new Pay();
                $pay->user_id = $request->user_id;
                $pay->balance = $request->balance;
                $pay->isIn = 0;
                $pay->status = 1;
                $pay->save();
                $admin = User::find(1);
                $admin->balance -= $pay->balance;
                $admin->save();
                return Redirect::route('gate::buyer')->with('notice', 'Thêm ngân sách vào đại lý thành công');
            } elseif ($request->has('addBalance')) {
                $user = User::find($request->user_id);
                $user->balance += (int)($request->balance);
                $user->save();
                $pay = new Pay();
                $pay->user_id = $request->user_id;
                $pay->balance = $request->balance;
                $pay->isIn = 1;
                $pay->status = 1;
                $pay->save();
                $admin = User::find(1);
                $admin->balance += $pay->balance;
                $admin->save();
                return Redirect::route('gate::buyer')->with('notice', 'Thêm ngân sách vào đại lý thành công');
            }
        }
    }

    public function pay(Request $request)
    {
        if ($request->isMethod('get')) {
            if ($request->has('check')) {
                $p = Pay::find($request->get('check'));

                if ($p->status == 0) {
                    $p->status = 1;
                    $p->save();
                    $admin = User::find(1);
                    $admin->balance -= $p->balance;
                    $admin->save();
                }
            }
            $pays = Pay::where('isIn', 0)->orderBy('id', 'desc')->get();
            return view('gate.pay', ['pays' => $pays]);
        }
    }

    public function rate(Request $request)
    {
        $networks = Network::all();
        if ($request->isMethod('get')) {
            return view('gate.rate', ['networks' => $networks]);
        } else {
            foreach ($networks as $net) {
                $net->rate_out = $request->get('rate_out')[$net->id];
                $net->rate_in = $request->get('rate_in')[$net->id];
                $net->rate_in_guess = $request->get('rate_in_guess')[$net->id];
                $net->save();
            }
            return Redirect::route('gate::rate')->with('notice', 'Cập nhập chiết khấu thành công');
        }
    }

    public function user_in(Request $request)
    {
        if ($request->isMethod('get')) {
            $users = User::where('isShop', 1)->paginate(30);
            if ($request->has('banned')) {
                $u = User::find($request->banned);
                $u->isBanned = !$u->isBanned;
                $u->save();
                return Redirect::route('gate::user_in')->with('notice', 'Tài khoản đã bị vô hiệu hóa');
            }
            return view('gate.user_in', ['users' => $users]);
        }
    }

    public function error(Request $request)
    {
        $error_cards = Card::where('isDone', 1)->where('isCorrect', 0)->orderBy('id', 'desc')->paginate(30);
        return view('gate.error', ['error_cards' => $error_cards]);
    }

    public function sales(Request $request)
    {
        $cards = Card::where('isDone', 1)->where('isCorrect', 1)->orderBy('id', 'desc')->paginate(30);
        return view('gate.sales', ['cards' => $cards]);

    }

    public function logout()
    {
        Auth::logout();
        return Redirect::route('gate::index');
    }

   
}
