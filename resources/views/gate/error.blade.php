@extends('gate.template')
@section('content')
    <div id="buy" class="pt-5 pb-5">
        <div class="container">

            @include('gate.navbar')

            <div class="row mt-3">
                <div class="col-sm-12">
                    <div class="bg-white p-3">
                        @if(\Illuminate\Support\Facades\Session::has('notice'))
                            <div class="col-sm-12 col-12">
                                <div class="alert alert-danger" role="alert">
                                    {{\Illuminate\Support\Facades\Session::get('notice')}}
                                </div>
                                <br>
                            </div>
                        @endif

                        <table class="datatable table table-striped table-bordered w-100">
                            <thead>
                            <tr>
                                <th width="10px">#ID</th>
                                <th>Mạng - Giá</th>
                                <th>Serial</th>
                                <th>Mã thẻ</th>
                                <th>Gửi bởi</th>

                                <th>Báo hủy bởi</th>
                                <th>Vào lúc</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($error_cards as $card)
                                <tr>
                                    <td>{{$card->id}}</td>
                                    <td>{{$card->network->name}} - {{number_format($card->price->price)}}</td>
                                    
                                    <td>{{$card->serial}}</td>
                                    <td>{{$card->key}}</td>
                                    @if($card->member_id <> 0)
                                        <td>{{$card->member->name}}</td>
                                    @else
                                        <td>Không có thành viên</td>
                                    @endif
                                    <td>{{$card->trans->user->name}}</td>
                                    <td>{{$card->updated_at->format('H:i:s d/m/Y')}}</td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <p class="text-center">
                            {!! $error_cards->render("pagination::bootstrap-4") !!}
                        </p>

                        <p class="alert-warning p-2"><i class="fa fa-bell-o"></i> Trang lịch sử giao dịch sẽ tự động làm
                            mới trang trong 10s. Để cập nhập tình trạng giao dịch.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection