@extends('gate.template')
@section('content')

    <div id="intro" class="pt-3 pb-3">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-12">
                    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img class="d-block w-100"
                                     src="{{asset('gate/images/banner1.png')}}"
                                     alt="First slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100"
                                     src="{{asset('gate/images/banner2.png')}}"
                                     alt="Second slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100"
                                     src="{{asset('gate/images/banner3.png')}}"
                                     alt="Third slide">
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleControls" role="button"
                           data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleControls" role="button"
                           data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
                <div class="col-sm-4 col-12">
                    <h4 class="text-white text-center text-uppercase p-3 bg1"><i class="fa fa-sign-in"></i> Đăng nhập hệ
                        thống</h4>
                    <div class="form-login">
                        <form method="post" action="{{route('gate::index')}}" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tài khoản</label>
                                <input type="text" class="form-control"
                                       aria-describedby="emailHelp" placeholder="Tài khoản" name="username">

                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Mật khẩu</label>
                                <input type="password" class="form-control"
                                       placeholder="Mật khẩu" name="password">
                            </div>
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-sign-in"></i> Đăng
                                nhập
                            </button>
                            <small class="form-text text-muted">Tài khoản đại lý được cấp duy nhất, vui
                                lòng bảo mật thông tin để tránh tình huống xấu. Chúng tôi sẽ không chịu trách nhiệm nào
                                nếu
                                tài khoản của quý khách bị tiết lộ<br>
                                Để trở thành đại lý vui lòng liên hệ với chung tôi qua email : admin@thenap365.com .
                            </small>
                        </form>
                        @if(\Illuminate\Support\Facades\Session::has('notice'))
                            <br>
                            <div class="alert alert-danger" role="alert">
                                {{\Illuminate\Support\Facades\Session::get('notice')}}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-sm-12 d-none d-sm-inline">
                    <div class="bg1 p-3">
                        <h4 class="text-white text-uppercase"><i class="fa fa-bank"></i> Đối tác của chúng tôi</h4>
                        <hr>
                        <div class="partner">
                            <img src="{{asset('gate/images/partner/1.png')}}" alt="">
                            <img src="{{asset('gate/images/partner/2.png')}}" alt="">
                            <img src="{{asset('gate/images/partner/3.jpg')}}" alt="">
                            <img src="{{asset('gate/images/partner/4.jpeg')}}" alt="">
                            <img src="{{asset('gate/images/partner/5.jpeg')}}" alt="">
                            <img src="{{asset('gate/images/partner/6.jpg')}}" alt="">
                            <img src="{{asset('gate/images/partner/7.jpg')}}" alt="">
                            <img src="{{asset('gate/images/partner/8.jpg')}}" alt="">
                            <img src="{{asset('gate/images/partner/9.jpg')}}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection