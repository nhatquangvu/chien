@extends('gate.template')
@section('content')
    <div id="buy" class="pt-5 pb-5">
        <div class="container">

            @include('gate.navbar')

            <div class="row mt-3">
                <div class="col-sm-12">
                    <div class="bg-white p-3 mt-3">
                        <h3>Danh sách tài khoản </h3>

                        <table class="datatable table table-striped table-bordered w-100">
                            <thead>
                            <tr>
                                <th width="10px">#ID</th>
                                <th>Tên</th>
                                <th>Ngân hàng</th>
                                <th>Tên tài khoản</th>
                                <th>Số tài khoản</th>
                                <th>Số tiền</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($pays as $pay)
                                @if($pay->user->isShop == 1)
                                    <tr>
                                        <td>{{$pay->id}}</td>
                                        <td>{{$pay->user->name}}</td>
                                        <td>{{$pay->user->bank_name}}</td>
                                        <td>{{$pay->user->bank_account_name}}</td>
                                        <td>{{$pay->user->bank_account}}</td>
                                        <td>{{number_format($pay->balance)}}</td>
                                        <td>
                                            @if($pay->status == 0)
                                            <a class="badge badge-success text-white"
                                               href="{{route('gate::pay',['check' => $pay->id])}}"><i class="fa fa-check"></i>
                                                Xác nhận</a>
                                                @else
                                                <i class="fa fa-check"></i> Thành công
                                            @endif
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection