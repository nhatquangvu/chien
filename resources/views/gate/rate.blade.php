@extends('gate.template')
@section('content')
    <div id="buy" class="pt-5 pb-5">
        <div class="container">

            @include('gate.navbar')
            <form action="{{route('gate::rate')}}" method="post">

                <div class="row mt-3">
                    @if(\Illuminate\Support\Facades\Session::has('notice'))
                        <div class="col-sm-12 col-12">
                            <br>
                            <div class="alert alert-success" role="alert">
                                {{\Illuminate\Support\Facades\Session::get('notice')}}
                            </div>
                            <br>
                        </div>
                    @endif
                    <div class="col-sm-4">
                        <div class="bg-white p-3 mb-5">
                            <h4>Chiết khấu bán ra</h4>
                            <hr>

                            @foreach($networks as $net)
                                <div class="form-group">
                                    <label for="">{{$net->name}} : <span class="badge badge-danger">Giá hiện tại : {{$net->rate_out}}
                                            %</span></label>
                                    <input type="number" class="form-control" name="rate_out[{{$net->id}}]" value="{{$net->rate_out}}" >
                                </div>
                            @endforeach
                        </div>

                    </div>

                    <div class="col-sm-4">
                        <div class="bg-white p-3 mb-5">
                            <h4>Nhập Shop</h4>
                            <hr>

                            @foreach($networks as $net)
                                <div class="form-group">
                                    <label for="">{{$net->name}} : <span class="badge badge-success">Giá hiện tại : {{$net->rate_in}}
                                            %</span></label>
                                    <input type="number" class="form-control" name="rate_in[{{$net->id}}]" value="{{$net->rate_in}}">

                                </div>
                            @endforeach
                        </div>

                    </div>

                    <div class="col-sm-4">
                        <div class="bg-white p-3 mb-5">
                            <h4>Nhập khách lẻ</h4>
                            <hr>

                            @foreach($networks as $net)
                                <div class="form-group">
                                    <label for="">{{$net->name}} : <span class="badge badge-primary">Giá hiện tại : {{$net->rate_in_guess}}
                                            %</span></label>
                                    <input type="number" class="form-control" name="rate_in_guess[{{$net->id}}]" value="{{$net->rate_in_guess}}">

                                </div>
                            @endforeach
                        </div>

                    </div>
                    <div class="col-sm-4 offset-sm-4">
                        {{csrf_field()}}
                        <button class="btn btn-block btn-info"><i class="fa fa-upload"></i> Cập nhập</button>
                    </div>

                </div>
            </form>

        </div>
    </div>
@endsection