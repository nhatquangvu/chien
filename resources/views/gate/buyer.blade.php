@extends('gate.template')
@section('content')
    <div id="buy" class="pt-5 pb-5">
        <div class="container">

            @include('gate.navbar')

            <div class="row mt-3">
                <div class="col-sm-8">
                    <div class="bg-white p-3">
                        @if(\Illuminate\Support\Facades\Session::has('notice'))
                            <div class="col-sm-12 col-12">
                                <div class="alert alert-success" role="alert">
                                    {{\Illuminate\Support\Facades\Session::get('notice')}}
                                </div>
                                <br>
                            </div>
                        @endif

                        <table class="datatable table table-striped table-bordered w-100">
                            <thead>
                            <tr>
                                <th width="10px">#ID</th>
                                <th>Tên</th>
                                <th>Tài khoản</th>
                                <th>Loại tài khoản</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td>{{$user->id}}</td>
                                    <td>{{$user->name}}</td>
                                    <td>{{number_format($user->balance)}}</td>
                                    @if($user->isVip == 1)
                                        <td>
                                            <span class="badge badge-danger">V.I.P</span>
                                            @if($user->isBanned == 1)
                                                <span class="badge badge-warning">Banned</span>
                                        </td>
                                    @endif
                                    @else
                                        <td>
                                            <span class="badge badge-success">Tài khoản thường</span>
                                            @if($user->isBanned == 1)
                                                <span class="badge badge-warning">Banned</span>
                                            @endif
                                        </td>
                                    @endif
                                    <td>
                                        <a href="{{route('gate::buyer',['vip'=> $user->id])}}"
                                           class="badge badge-danger"><i class="fa fa-upload"></i> Lên / Bỏ VIP</a>
                                        <a href="{{route('gate::buyer',['banned'=> $user->id])}}"
                                           class="badge badge-warning"><i class="fa fa-trash"></i> Banned / Unbanned</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <br>

                    </div>
                    <div class="bg-white p-3 mt-3">
                        <h5>Lịch sử giao dịch</h5>

                        @foreach($pays as $pay)
                            @if($pay->isIn == 1)
                                > Thêm <span class="badge badge-success">{{number_format($pay->balance)}}</span> vào tài
                            khoản <span class="badge badge-success">{{$pay->user->name}}</span> vào {{$pay->created_at->format('h:i:s d/m/Y')}}
                            <br>
                            @else
                                > Rút lại <span class="badge badge-danger">{{number_format($pay->balance)}}</span> từ tài
                            khoản <span class="badge badge-danger">{{$pay->user->name}}</span> vào {{$pay->created_at->format('h:i:s d/m/Y')}}
                            <br>
                                @endif
                        @endforeach
                        <p>{{ $pays->links("pagination::bootstrap-4") }}</p>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="bg-white p-3 mb-2">
                        <h3>Thêm tài khoản</h3>
                        <hr>
                        <form action="{{route('gate::buyer')}}" method="post">
                            <input type="hidden" name="addUser" value="1">
                            <label for="">Số điện thoại</label>
                            <input type="text" class="form-control" name="name" placeholder="Số điện thoại" required>
                            <label for="">Mật khẩu</label>
                            <input type="text" class="form-control" name="password" placeholder="Mật khẩu" required>
                            <label for="">Mật khẩu</label>
                            <select class="form-control" name="isVip">
                                <option value="1">Tài khoản VIP</option>
                                <option value="0" selected>Tài khoản Thường</option>
                            </select>
                            <br>
                            <button type="submit" class="btn btn-primary"><i class="fa fa-key"></i> Thêm người dùng
                            </button>
                            {{csrf_field()}}
                        </form>
                    </div>
                    <div class="bg-white p-3 mb-2">
                        <h3>Nạp tiền đại lý</h3>
                        <hr>
                        <form action="{{route('gate::buyer')}}" method="post">
                            <input type="hidden" name="addBalance" value="1">
                            <select class="form-control" name="user_id">
                                @foreach($users as $user)
                                    @if($user->isBanned == 0)
                                        <option value="{{$user->id}}">{{$user->name}}</option>
                                    @endif
                                @endforeach
                            </select>
                            <label for="">Số tiền</label>
                            <input type="number" class="form-control" name="balance" placeholder="Số tiền" required>
                            <br>
                            <button type="submit" class="btn btn-success "><i class="fa fa-key"></i> Thêm ngân sách
                            </button>
                            {{csrf_field()}}
                        </form>
                    </div>
                    <div class="bg-white p-3 mb-2">
                        <h3>Rút tiền đại lý</h3>
                        <hr>
                        <form action="{{route('gate::buyer')}}" method="post">
                            <input type="hidden" name="removeBalance" value="1">
                            <select class="form-control" name="user_id">
                                @foreach($users as $user)
                                    @if($user->isBanned == 0)
                                        <option value="{{$user->id}}">{{$user->name}}</option>
                                    @endif
                                @endforeach
                            </select>
                            <label for="">Số tiền</label>
                            <input type="number" class="form-control" name="balance" placeholder="Số tiền" required>
                            <br>
                            <button type="submit" class="btn btn-danger "><i class="fa fa-key"></i> Rút tiền tài khoản
                            </button>
                            {{csrf_field()}}
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection