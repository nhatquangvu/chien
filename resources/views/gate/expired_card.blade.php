@extends('gate.template')
@section('content')
    <meta http-equiv="refresh" content="10">
    <div id="buy" class="pt-5 pb-5">
        <div class="container">

            @include('gate.navbar')

            <div class="row mt-3">
                <div class="col-sm-12">
                    <div class="bg-white p-3">
                        @if(\Illuminate\Support\Facades\Session::has('notice'))
                            <div class="col-sm-12 col-12">
                                <div class="alert alert-danger" role="alert">
                                    {{\Illuminate\Support\Facades\Session::get('notice')}}
                                </div>
                                <br>
                            </div>
                        @endif

                        <table class="datatable table table-striped table-bordered w-100">
                            <thead>
                            <tr>
                                <th width="10px">#ID</th>
                                <th>Mạng</th>
                                <th>Mệnh giá</th>
                                <th>Serial</th>
                                <th>Mã thẻ</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($expired_cards as $card)
                                <tr>
                                    <td>{{$card->id}}</td>
                                    <td>{{$card->network->name}}</td>
                                    <td>{{$card->price->price}}</td>
                                    <td>{{$card->serial}}</td>
                                    <td>{{$card->key}}</td>
                                    <td><a href="{{route('gate::expired_card',['back' => $card->id])}}"
                                           class="badge badge-success text-uppercase text-white"><i
                                                    class="fa fa-dollar"></i> Nhập thẻ giá API</a>
                                            <a href="{{route('gate::expired_card',['error' => $card->id])}}"
                                                class="badge badge-danger text-uppercase text-white"><i
                                                    class="fa fa-trash-o"></i> Hủy thẻ</a></td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <p class="text-center">
                            {!! $expired_cards->render("pagination::bootstrap-4") !!}
                        </p>

                        <p class="alert-warning p-2"><i class="fa fa-bell-o"></i> Trang lịch sử giao dịch sẽ tự động làm
                            mới trang trong 10s. Để cập nhập tình trạng giao dịch.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection