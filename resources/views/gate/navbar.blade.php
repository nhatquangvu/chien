@php
    $networks = \App\Network::all();
@endphp
<div class="row">
    <div class="col-12 mb-1">
        <a href="{{route('gate::dashboard')}}" class="text-uppercase"><i class="fa fa-home"></i> Trang chính</a>
        @if(\Illuminate\Support\Facades\Auth::user()->isAdmin)
            <a href="{{route('gate::expired_card')}}" class="ml-2 text-uppercase"><i class="fa fa-bank"></i> Thẻ hết hạn</a>
            <a href="{{route('gate::buyer')}}" class="ml-2 text-uppercase"><i class="fa fa-shopping-bag"></i> Đại lý</a>
            <a href="{{route('gate::pay')}}" class="ml-2 text-uppercase"><i class="fa fa-money"></i> Thanh toán</a>
        @endif
    </div>
    <div class="col-sm-6 col-12 d-none d-sm-block">
        <span class="discount-viettel">Viettel : {{$networks[0]->rate_out}}%</span>
        <span class="discount-vinaphone">Vina : {{$networks[1]->rate_out}}% </span>
        <span class="discount-mobiphone">Mobi : {{$networks[2]->rate_out}}% </span>
    </div>
    <div class="col-sm-6 col-12 text-right mt-2">
                    <span class="balance">
                       {{\Illuminate\Support\Facades\Auth::user()->name}} <a href="{{route('gate::logout')}}"
                                                                             class="badge badge-danger text-white"><i
                                    class="fa fa-sign-out"></i> Thoát</a> - <span>Tài khoản: {{number_format(\Illuminate\Support\Facades\Auth::user()->balance)}}</span>
                    </span>
    </div>
</div>