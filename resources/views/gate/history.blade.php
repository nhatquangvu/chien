@extends('gate.template')
@section('content')
<meta http-equiv="refresh" content="300" >
    <div id="buy" class="pt-5 pb-5">
        <div class="container">

            @include('gate.navbar')

            <div class="row mt-3">
                <div class="col-sm-12">
                    <div class="bg-white p-3">
                        @if(\Illuminate\Support\Facades\Session::has('notice'))
                            <div class="col-sm-12 col-12">
                                <div class="alert alert-danger" role="alert">
                                    {{\Illuminate\Support\Facades\Session::get('notice')}}
                                </div>
                                <br>
                            </div>
                        @endif
                        <p>
                            <a href="{{route('gate::history')}}" class="btn btn-xs btn-primary"><i class="fa fa-clock-o"></i> Tất cả</a>
                            <a href="{{route('gate::history',['isDone' => 1])}}" class="btn btn-xs btn-success"><i class="fa fa-check"></i> Thành công</a>
                        </p>
                        <table class="datatable table table-striped table-bordered w-100">
                            <thead>
                            <tr>
                                <th width="10px">#ID</th>
                                <th>Mạng</th>
                                <th>Mệnh giá</th>
                                <th>Serial</th>
                                <th>Mã thẻ</th>
                                <th>Thời gian</th>
                                <th>Trạng thái</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($transaction as $trans)
                                @if($trans->isUsed == 0 && $trans->card_id <> 0 )
                                <tr  class="table-danger">
                                @else
                                <tr>
                                @endif
                                    <td>{{$trans->id}}</td>
                                    <td>{{$trans->network->name}}</td>
                                    <td>{{number_format($trans->price->price)}}</td>

                                    @if($trans->card_id <> 0 && $trans->card)
                                        <td>{{$trans->card->serial}}</td>
                                        <td>{{$trans->card->key}}</td>
                                        <td>{{$trans->updated_at->format('H:i:s d/m/Y')}}</td>

                                        <td>
                                            <span class="badge badge-success"><i class="fa fa-check"></i> Đã nhận mã thẻ</span>
                                        </td>
                                        <td>
                                            @if($trans->isUsed == 0)
                                                <a href="{{route('gate::history',['check' => $trans->id])}}" class="badge badge-success"><i class="fa fa-ticket"></i>Thẻ đúng</a>
                                                <a href="{{route('gate::history',['error' => $trans->id])}}" class="badge badge-danger btn-confirm-error"><i class="fa fa-ticket"></i> Báo sai</a>
                                                @elseif($trans->isUsed == 1 && $trans->card->isCorrect == 0)
                                                <span class="badge badge-danger">Thẻ hủy</span>
                                                <a href="{{route('gate::history',['recheck' => $trans->id])}}" class="badge badge-success"><i class="fa fa-ticket"></i>Hoàn thẻ</a>

                                            @else
                                                Thẻ đã sử dụng
                                            @endif
                                        </td>
                                    @else
                                        <td>....</td>
                                        <td>....</td>
                                        <td>....</td>
                                        <td>
                                            <span class="badge badge-warning"><i class="fa fa-clock-o"></i> Đang chờ </span>
                                        </td>
                                        <td>
                                            <a href="{{route('gate::history',['del' => $trans->id])}}" class="badge badge-danger">Xóa chờ</a>
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <p class="text-center">
                            {!! $transaction->render("pagination::bootstrap-4") !!}
                        </p>

                        <p class="alert-warning p-2"><i class="fa fa-bell-o"></i> Trang lịch sử giao dịch sẽ tự động làm mới trang trong 10s. Để cập nhập tình trạng giao dịch.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection