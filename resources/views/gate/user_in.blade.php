@extends('gate.template')
@section('content')
    <div id="buy" class="pt-5 pb-5">
        <div class="container">

            @include('gate.navbar')

            <div class="row mt-3">
                <div class="col-sm-12">
                    <div class="bg-white p-3">
                        @if(\Illuminate\Support\Facades\Session::has('notice'))
                            <div class="col-sm-12 col-12">
                                <div class="alert alert-success" role="alert">
                                    {{\Illuminate\Support\Facades\Session::get('notice')}}
                                </div>
                                <br>
                            </div>
                        @endif

                        <table class="datatable table table-striped table-bordered w-100">
                            <thead>
                            <tr>
                                <th width="10px">#ID</th>
                                <th>Tên</th>
                                <th>Tài khoản</th>
                                <th>Loại tài khoản</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td>{{$user->id}}</td>
                                    <td>{{$user->name}}</td>
                                    <td>{{number_format($user->balance)}}</td>
                                    @if($user->isBanned != 1)
                                        <td>
                                                <span class="badge badge-primary">Hoạt động</span>
                                        </td>

                                    @else
                                        <td>
                                                <span class="badge badge-warning">Banned</span>
                                        </td>
                                    @endif
                                    <td>

                                        <a href="{{route('gate::user_in',['banned'=> $user->id])}}"
                                           class="badge badge-warning"><i class="fa fa-trash"></i> Banned / Unbanned</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <br>
                        {{$users->links()}}

                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection