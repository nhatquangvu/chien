<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Thenap365 - Đồng hành cùng quý đại lý. Phát triển thần tốc</title>
    <link rel="icon" href="{{asset('gate/images/icon.png')}}">

    <link rel="stylesheet" href="{{asset('/gate/css')}}/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('/gate/css')}}/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/v/bs4/dt-1.10.18/fh-3.1.4/r-2.2.2/datatables.min.css"/>

<body>
<div id="top" class="bg1 text-white">
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-12">
                <h2 class="text-uppercase">thenap365.com</h2>
                <span class="text-uppercase">Giải pháp tiết kiệm chi phí tối đa</span>
            </div>
            <div class="col-sm-8 col-12 text-right font-weight-bold">
                <div><i class="fa fa-phone-square"></i> 0911 xxx xxx <i class="fa fa-envelope ml-2"></i>
                    admin@thenap365.com
                    <a href="mailto:admin@thenap365.com" class="btn-register ml-2 d-none d-sm-inline"> <i class="fa fa-handshake-o"></i> Đăng ký
                        đại lý</a>
                </div>
                <ul>
                    <li>
                        <a href="{{route('gate::index')}}"><i class="fa fa-home"></i> Trang chủ</a>
                        <a href="mailto:admin@thenap365.com">Giới thiệu</a>
                        <a href="mailto:admin@thenap365.com">Liên hệ</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div id="notice">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <marquee behavior="" direction="">
                    <i class="fa fa-bullhorn"></i>
                    Thenap365 - Cam kết mang đến quý đại lý sản phẩm chất lượng nhất, mức chiết khâu cao nhất thị
                    trường. Với kinh nghiệm lâu năm kinh doanh và am hiểu thị trường, chúng tôi mong muốn mang đến cho
                    quý đại lý lợi ích tối đa.
                </marquee>
            </div>
        </div>
    </div>
</div>
@yield('content')
<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <h2>thenap365.com</h2>
                <p class="text-justify">Mọi bản quyền thuộc về thenap365.com. Mọi thông tin quản quyền về nội dung
                    vui lòng liên hệ admin@thenap365.com để
                    được cung cấp bản quyền nội dung.</p>
            </div>
            <div class="col-sm-4">
                <h4 class="text-uppercase">Chính sách khách hàng</h4>
                <hr>
                <p>
                    <a href="#"><i class="fa fa-bullseye"></i> Chính sách đại lý</a> <br>
                    <a href="#"><i class="fa fa-bullseye"></i> Chính sách khách hàng</a> <br>
                    <a href="#"><i class="fa fa-bullseye"></i> Chính sách bán hàng</a> <br>
                    <a href="#"><i class="fa fa-bullseye"></i> Cam kế người dùng</a> <br>
                </p>
            </div>
            <div class="col-sm-4">
                <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fthenap365&tabs=timeline&width=340&height=200&small_header=true&adapt_container_width=true&hide_cover=true&show_facepile=false&appId=890982847703106"
                        width="340" height="200" style="border:none;overflow:hidden" scrolling="no" frameborder="0"
                        allowTransparency="true" allow="encrypted-media"></iframe>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="{{asset('gate/js')}}/bootstrap.bundle.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
<script type="text/javascript"
        src="https://cdn.datatables.net/v/bs4/dt-1.10.18/fh-3.1.4/r-2.2.2/datatables.min.js"></script>
<script src="{{asset('gate/js/script.js')}}"></script>

</body>
</html>