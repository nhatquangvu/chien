@extends('gate.template')
@section('content')

    <div id="dashboard" class="pt-5 pb-5">
        <div class="container">
            @include('gate.navbar')
            @if(! \Illuminate\Support\Facades\Auth::user()->isAdmin)
                <div class="row mt-2">
                    <div class="col-sm-4 col-12">
                        <a href="{{route('gate::buy')}}">
                            <div class="dashboard-icon">
                                <i class="fa fa-credit-card"></i>
                                <br>
                                <h5>Mua thẻ điện thoại</h5>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-4 col-12">
                        <a href="{{route('gate::history')}}">
                            <div class=" dashboard-icon">
                                <i class="fa fa-bank"></i>
                                <br>
                                <h5>Lịch sử giao dịch</h5>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-4 col-12">
                        <a href="{{route('gate::user')}}">
                            <div class="dashboard-icon">
                                <i class="fa fa-user-circle-o"></i>
                                <br>
                                <h5>Thông tin tài khoản</h5>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-12">
                        <div class="bg-danger p-3 text-white">
                            <h4><i class="fa fa-bell-o"></i> Chú ý</h4>
                            <p>
                                <i class="fa fa-check"></i> Mã thẻ sau khi nhận 10 phút sẽ tự động hoàn thành giao dịch
                                <br><i class="fa fa-check"></i> Không nhận lại thẻ sau khi thanh toán
                                <br><i class="fa fa-check"></i> Gian lân trong việc check thẻ sẽ bị banned tài khoản vĩnh viễn, không hoàn trả số dư
                            </p>
                        </div>
                    </div>
                </div>
            @endif
            @if(\Illuminate\Support\Facades\Auth::user()->isAdmin)
                <div class="row mt-3">
                    <div class="col-12">
                        <h4>Admin Area</h4>
                        <hr>
                    </div>

                    <div class="col-sm-3 col-6">
                        <a href="{{route('gate::error')}}">
                            <div class=" dashboard-icon">
                                <i class="fa fa-heartbeat"></i>
                                <br>
                                <h5>Thẻ gạch sai
                                </h5>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-3 col-6">
                        <a href="{{route('gate::expired_card')}}">
                            <div class=" dashboard-icon">
                                <i class="fa fa-heartbeat"></i>
                                <br>
                                <h5>Thẻ hết hạn
                                </h5>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-3 col-6">
                        <a href="{{route('gate::buyer')}}">
                            <div class="dashboard-icon">
                                <i class="fa fa-shopping-bag"></i>
                                <br>
                                <h5>Đại lý</h5>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-3 col-6">
                        <a href="{{route('gate::pay')}}">
                            @if(\App\Pay::where('status',0)->count() == 0)
                                <div class="dashboard-icon">
                                    @else
                                        <div class="dashboard-icon animated pulse infinite">
                                            @endif
                                            <i class="fa fa-id-card"></i>
                                            <br>
                                            <h5>Thanh Toán</h5>
                                        </div>
                        </a>
                    </div>
                    <div class="col-sm-3 col-6">
                        <a href="{{route('gate::rate')}}">
                            <div class="dashboard-icon">
                                <i class="fa fa-bar-chart"></i>
                                <br>
                                <h5>Chiết khấu</h5>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-3 col-6">
                        <a href="{{route('gate::user_in')}}">
                            <div class="dashboard-icon">
                                <i class="fa fa-pagelines"></i>
                                <br>
                                <h5>Tài khoản shop</h5>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-3 col-6">
                        <a href="{{route('gate::sales')}}">
                            <div class=" dashboard-icon">
                                <i class="fa fa-heartbeat"></i>
                                <br>
                                <h5>Lợi nhuận
                                </h5>
                            </div>
                        </a>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
