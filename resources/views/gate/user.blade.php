@extends('gate.template')
@section('content')
    <div id="buy" class="pt-5 pb-5">
        <div class="container">

            @include('gate.navbar')

            <div class="row mt-3">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-12 col-sm-6 offset-sm-3">
                            <div class="bg-white p-4">

                                <h4>Thông tin tài khoản</h4>
                                <hr>
                                <p><span class="font-weight-bold">Tên tài khoản</span>
                                    : {{\Illuminate\Support\Facades\Auth::user()->name}}</p>
                                <p><span class="font-weight-bold">Email</span>
                                    : {{\Illuminate\Support\Facades\Auth::user()->email}}</p>
                                <p><span class="font-weight-bold">Tài khoản</span>
                                    : {{number_format(\Illuminate\Support\Facades\Auth::user()->balance)}}</p>
                                <p><span class="font-weight-bold">Loại tài khoản</span> :
                                    @if(\Illuminate\Support\Facades\Auth::user()->isVip == 1)
                                        <span class="badge-danger badge">V.I.P</span>
                                    @else
                                        <span class="badge badge-success">Tài khoản thường</span>
                                    @endif
                                </p>

                                <h4>Đổi mật khẩu</h4>
                                <hr>
                                @if(\Illuminate\Support\Facades\Session::has('notice'))
                                    <div class="col-sm-12 col-12">
                                        <div class="alert alert-danger" role="alert">
                                            {{\Illuminate\Support\Facades\Session::get('notice')}}
                                        </div>
                                        <br>
                                    </div>

                                @endif
                                <form action="{{route('gate::user')}}" method="post">
                                    <label for="">Mật khẩu mới</label>
                                    <input type="password" class="form-control" name="password" placeholder="Mật khẩu mới" required>
                                    <label for="">Gõ lại mật khẩu</label>
                                    <input type="password" class="form-control" name="repassword" placeholder="Mật khẩu mới" required>
                                    <br>
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-key"></i> Thay đổi mật
                                        khẩu
                                    </button>
                                    {{csrf_field()}}
                                </form>
                                <br>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection