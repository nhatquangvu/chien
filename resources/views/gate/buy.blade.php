@extends('gate.template')
@section('content')

    <div id="buy" class="pt-5 pb-5">
        <div class="container">

            @include('gate.navbar')

            <div class="row mt-3">
                @if(\Illuminate\Support\Facades\Session::has('notice'))
                    <div class="col-sm-12 col-12">
                        <div class="alert alert-danger" role="alert">
                            {{\Illuminate\Support\Facades\Session::get('notice')}}
                        </div>
                        <br>
                    </div>

                @endif
                @foreach($networks as $net)
                    <div class="col-sm-4">
                        <div class="bg-white p-3">
                            <h4 class="text-center text-uppercase">{{$net->name}}</h4>
                            <hr>
                            <div class="row">
                                @foreach($prices as $price)
                                    @if(\Illuminate\Support\Facades\Auth::user()->isVip)
                                        @php
                                            $quality =   \App\Card::where('price_id',$price->id)
                                                                    ->where('network_id',$net->id)
                                                                    ->where('isDone',0)
                                                                    ->count();
                                        @endphp
                                        <div class="col-sm-4 col-6">
                                            <a href="/buy/{{$net->id}}/{{$price->id}}" class="buy-card">
                                                @if($quality == 0)
                                                    <div class="bg-price text-center">
                                                        @else
                                                            <div class="bg-price-has text-center">
                                                                @endif
                                                                <i>{{number_format($price->price)}}</i> <br>
                                                                <span>Kho: {{$quality}}</span>
                                                            </div>
                                            </a>
                                        </div>
                                    @else
                                        @if($price->id <5)
                                            @php
                                                $quality =   \App\Card::where('price_id',$price->id)
                                                                        ->where('network_id',$net->id)
                                                                        ->where('isDone',0)
                                                                        ->count();
                                            @endphp
                                            <div class="col-sm-4 col-6">
                                                <a href="/buy/{{$net->id}}/{{$price->id}}" class="buy-card">
                                                    @if($quality == 0)
                                                        <div class="bg-price text-center">
                                                            @else
                                                                <div class="bg-price-has text-center">
                                                                    @endif
                                                                    <i>{{number_format($price->price)}}</i> <br>
                                                                    <span>Kho: {{$quality}}</span>
                                                                </div>
                                                </a>
                                            </div>
                                        @endif
                                    @endif

                                @endforeach
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            @if(!\Illuminate\Support\Facades\Auth::user()->isVip)
                <div class="row mt-2">
                    <div class="col-12">
                        <p class="p-3 bg-success text-white font-weight-bold text-uppercase"><i class="fa fa-bell"></i>
                            Chỉ những tài khoản VIP mới
                            được phép mua các the cào mệnh giá 200.000 và 500.000. Xin quý khách thông cảm vì
                            sự bất tiện này</p>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection