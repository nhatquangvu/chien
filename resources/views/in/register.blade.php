<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('in_1/assets')}}/images/favicon.png">
    <title>Admin Press Admin Template - The Ultimate Bootstrap 4 Admin Template</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{asset('in_1/assets')}}/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{asset('in_1/assets')}}/css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="{{asset('in_1/assets')}}/css/colors/blue.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<!-- ============================================================== -->
<!-- Preloader - style you can find in spinners.css -->
<!-- ============================================================== -->
<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
    </svg>
</div>
<!-- ============================================================== -->
<!-- Main wrapper - style you can find in pages.scss -->
<!-- ============================================================== -->
<section id="wrapper">
    <div class="login-register"
         style="background-image:url({{asset('in_1/assets')}}/images/background/login-register.jpg);">
        <div class="login-box card">
            <div class="card-body">
                <form class="form-horizontal form-material" method="post" action="{{route('in::register')}}">
                    <h3 class="box-title m-b-20">Đăng ký</h3>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" required="true" placeholder="Tên đăng nhập" name="name" >
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input class="form-control" type="password" required="true" placeholder="Mật khẩu" name="password">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input class="form-control" type="password" required="true" placeholder="Gõ lại mật khẩu" name="repassword">
                        </div>
                    </div>
                    {{csrf_field()}}
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light"
                                    type="submit"><i class="fa fa-sign-in"></i> Đăng ký
                            </button>
                        </div>
                    </div>
                    <i class="fa fa-bell"></i> Bạn có thể <a href="{{route('in::login')}}">Đăng nhập</a>
                </form>
                @if(\Illuminate\Support\Facades\Session::has('notice'))
                    <div class="col-sm-12 col-12">
                        <br>
                        <div class="alert alert-danger" role="alert">
                            {{\Illuminate\Support\Facades\Session::get('notice')}}
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</section>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
<script src="{{asset('in_1/assets')}}/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="{{asset('in_1/assets')}}/plugins/bootstrap/js/popper.min.js"></script>
<script src="{{asset('in_1/assets')}}/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="{{asset('in_1/assets')}}/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="{{asset('in_1/assets')}}/js/waves.js"></script>
<!--Menu sidebar -->
<script src="{{asset('in_1/assets')}}/js/sidebarmenu.js"></script>
<!--stickey kit -->
<script src="{{asset('in_1/assets')}}/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
<script src="{{asset('in_1/assets')}}/plugins/sparkline/jquery.sparkline.min.js"></script>
<!--Custom JavaScript -->
<script src="{{asset('in_1/assets')}}/js/custom.min.js"></script>
<!-- ============================================================== -->
<!-- Style switcher -->
<!-- ============================================================== -->
<script src="{{asset('in_1/assets')}}/plugins/styleswitcher/jQuery.style.switcher.js"></script>
</body>

</html>