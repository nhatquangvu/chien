@extends('in.template')
@section('content')
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Thông tin ngân hàng</h3>
        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12 col-sm-6">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title text-uppercase">Thay đổi thông tin ngân hàng</h4>
                        <hr>
                        <p>Ngân hàng : <span class="font-weight-bold">{{\Illuminate\Support\Facades\Auth::user()->bank_name}}</span></p>
                        <p>Chủ TK : <span class="font-weight-bold">{{\Illuminate\Support\Facades\Auth::user()->bank_account_name}}</span></p>
                        <p>Số tài khoản : <span class="font-weight-bold">{{\Illuminate\Support\Facades\Auth::user()->bank_account}}</span></p>
                        @if(\Illuminate\Support\Facades\Session::has('notice'))
                            <div class="col-sm-12 col-12">
                                <br>
                                <div class="alert alert-success" role="alert">
                                    {{\Illuminate\Support\Facades\Session::get('notice')}}
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title text-uppercase">Thay đổi thông tin</h4>
                        <hr>
                        <form action="{{route('in::bank')}}" method="post">
                            <div class="form-group">
                                <label for="">Tên ngân hàng</label>
                                <input type="text" class="form-control" name="bank_name" required>
                            </div>
                            <div class="form-group">
                                <label for="">Tên chủ tài khoản</label>
                                <input type="text" class="form-control" name="bank_account_name" required>
                            </div>
                            <div class="form-group">
                                <label for="">Số tài khoản</label>
                                <input type="text" class="form-control" name="bank_account" required>
                            </div>
                            {{csrf_field()}}
                            <button class="btn btn-default btn-info  btn-block"><i class="fa fa-upload"></i> Cập nhập</button>
                        </form>
                    </div>
                </div>


            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->

    </div>
    <script>
        function click_btn_add() {
            $('#form_add_card').toggle();
            $('#manager_card').toggle();
            $('#btn_add_card').toggle();
            $('#btn_manager_card').toggle();
        }
    </script>
@endsection