@extends('in.template')
@section('content')
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Thẻ cào</h3>
        </div>


    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title text-uppercase">Mức chiết khấu thay đổi theo thời gian thực</h4>
                        <p>
                            - Chúng tôi thay đổi mức chiết khấu cho từng loại thẻ theo thời gian thực <br>
                            - Bạn hoàn toàn có thể quy đổi ra tiền mặt bất kỳ lúc nào nếu muốn <br>
                            - Tài khoản của bạn luôn được bảo mật 100% <br>
                            - Chúng tôi cam kết giữ kín toàn bộ thông tin khách hàng.
                        </p>
                        <div class="row m-t-40">
                            @foreach(\App\Network::all() as $net)
                                <div class="col-md-6 col-lg-4 col-xlg-4">
                                    <div class="card card-danger">
                                        <div class="box text-uppercase text-center text-white">
                                            <h6 class="text-white">Mức chiết khấu</h6>
                                            @if(\Illuminate\Support\Facades\Auth::user()->isVip == 0)
                                                <h1 class="font-light font-weight-bold text-white">{{$net->rate_in_guess}}
                                                    %</h1>
                                            @else
                                                <h1 class="font-light font-weight-bold text-white">{{$net->rate_in}}
                                                    %</h1>
                                            @endif

                                            <h4 class="text-white font-weight-bold">{{$net->name}}</h4>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>


                        <button type="button" class="btn btn-info btn-rounded" id="btn_add_card"
                                onclick="click_btn_add()">Thêm thẻ cào
                        </button>
                        <button type="button" class="btn btn-info btn-rounded" id="btn_manager_card"
                                style="display: none" onclick="click_btn_add()">Danh sách thẻ cào
                        </button>

                        @if(\Illuminate\Support\Facades\Session::has('notice'))

                            <div class="col-sm-12 col-12">
                                <br>
                                <div class="alert alert-success" role="alert">
                                    {{\Illuminate\Support\Facades\Session::get('notice')}}
                                </div>
                                <br>
                            </div>
                        @endif
                        <div id="form_add_card" style="display: none">
                            <div class="row">
                                <div class="col-sm-6 offset-sm-3">
                                    <h5>Thêm thẻ cào</h5>
                                    <form action="{{route('in::dashboard')}}" method="post"
                                          enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label for="">Nhà mạng</label>
                                            <select name="network_id" id="" class="form-control" required>
                                                @foreach(\App\Network::all() as $item)
                                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Mệnh giá</label>
                                            <select name="price_id" id="" class="form-control" required>
                                                @foreach(\App\Price::all() as $item)
                                                    <option value="{{$item->id}}">{{number_format($item->price)}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Số serial</label>
                                            <input type="text" name="serial" placeholder="Số serial"
                                                   class="form-control" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Mã thẻ</label>
                                            <input type="text" name="key" placeholder="Mã thẻ" class="form-control"
                                                   required>
                                        </div>
                                        <button type="submit" class="btn btn-block btn-info"><i
                                                    class="fa fa-id-card-o"></i> Thêm card
                                        </button>
                                        {{csrf_field()}}
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive" id="manager_card">
                            <table id="demo-foo-addrow" class="table m-t-30 table-hover no-wrap contact-list"
                                   data-page-size="10">
                                <thead>
                                <tr>
                                    <th>ID #</th>
                                    <th>Nhà mạng</th>
                                    <th>Mệnh giá</th>
                                    <th>Số serial</th>
                                    <th>Mã thẻ</th>
                                    <th>Thời gian</th>
                                    <th>Tình trạng thẻ</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($cards as $card)
                                    <tr>
                                        <td>{{$card->id}}</td>
                                        <td>{{$card->network->name}}</td>
                                        <td>{{number_format($card->price->price)}}</td>
                                        <td>{{$card->serial}}</td>
                                        <td>{{$card->key}}</td>
                                        <td>{{$card->created_at->format('h:i d/m/Y')}}</td>

                                        @if($card->isDone == 0)
                                            <td><span class="badge badge-warning">Chờ xử lý</span></td>
                                        @else
                                            @if($card->trans)
                                                @if($card->isDone == 1 && $card->trans->isUsed == 0)
                                                    <td><span class="badge badge-warning">Chờ xử lý</span></td>
                                                @elseif($card->isDone  == 1 && $card->trans->isUsed == 1 && $card->isCorrect == 1)
                                                    <td><span class="badge badge-success">Thành công</span></td>
                                                @else
                                                    <td><span class="badge badge-danger">Thẻ hủy</span></td>
                                                @endif
                                            @else
                                                <td><span class="badge badge-warning">Chờ xử lý</span></td>

                                            @endif

                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>

                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->

    </div>
    <script>
        function click_btn_add() {
            $('#form_add_card').toggle();
            $('#manager_card').toggle();
            $('#btn_add_card').toggle();
            $('#btn_manager_card').toggle();
        }
    </script>
@endsection