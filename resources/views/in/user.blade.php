@extends('in.template')
@section('content')
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Thông tin người dùng</h3>
        </div>


    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">

            <div class="col-12 col-sm-6 offset-sm-3">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title text-uppercase">Thay đổi thông tin</h4>
                        <hr>
                        <form action="{{route('in::user')}}" method="post">
                            <div class="form-group">
                                <label for="">Mật khẩu mới</label>
                                <input type="password" class="form-control" name="password">
                            </div>
                            <div class="form-group">
                                <label for="">Gõ lại mật khẩu mới</label>
                                <input type="password" class="form-control" name="repassword">
                            </div>
                            {{csrf_field()}}
                            <button class="btn btn-default btn-info btn-block"><i class="fa fa-upload"></i> Cập nhập</button>
                        </form>
                    </div>
                </div>


            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->

    </div>
    <script>
        function click_btn_add() {
            $('#form_add_card').toggle();
            $('#manager_card').toggle();
            $('#btn_add_card').toggle();
            $('#btn_manager_card').toggle();
        }
    </script>
@endsection