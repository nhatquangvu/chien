<!doctype html>
<html lang="en">
<head>
    <title>CỔNG THẺ 365 - SÀN GIAO DỊCH THẺ CÀO UY TÍN HÀNG ĐẦU VIỆT NAM</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="{{asset('in_1/images/logo.png')}}">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('in_1')}}/css/style.css">
</head>
<body>
<div id="top">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <span class="text-uppercase">Cổng trao đổi và giao dịch thẻ cào điện thoại</span>
            </div>
            <div class="col-sm-6">
                <span class="float-right">Hotline: 0382 645 480</span>
            </div>
        </div>
    </div>
</div>
<div class="bg-topbar">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <img src="{{asset('in_1')}}/images/logo-2.png" alt="" class="img-logo">
            </div>
            <div class="col-sm-6">
                <a href="{{route('in::login')}}" class="btn btn-sm btn-success text-white mt-4 float-right text-uppercase  ml-2"><i class="fa fa-sign-in"></i> Đăng nhập</a>
                <a href="{{route('in::register')}}" class="btn btn-sm btn-danger text-white mt-4 float-right text-uppercase ml-2"><i class="fa fa-user"></i> Đăng ký</a>
            </div>
        </div>
    </div>
</div>
<div id="menu">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <ul>
                    <li><a href="">Trang chủ</a></li>
                    <li><a href="#discount">Chính sách</a></li>
                    <li><a href="#guess">Trách nhiệm khách hàng</a></li>

                </ul>
            </div>
        </div>
    </div>
</div>

<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img class="d-block w-100" src="{{asset('in_1')}}/images/s1.png" alt="First slide">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="{{asset('in_1')}}/images/s2.png" alt="Second slide">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="{{asset('in_1')}}/images/s3.png" alt="Second slide">
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>

<div id="discount">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 p-0">
                <img src="{{asset('in_1')}}/images/banner1.png" alt="" class="img-fluid">
            </div>
            <div class="col-sm-6 pt-3 bg-brow">
                <h4 class="text-center text-uppercase">Về chúng tôi</h4>
                <hr>
                <p class="text-justify">Congthe365 là website cung cấp dịch vụ chuyển đổi thẻ nạp thành tiền mặt hoàn toàn tự động cho mọi người với mức chiết khấu thấp nhất và thanh toán nhanh nhất. <br>
                    Thời gian nhận thanh toán nhanh trong vòng 5 – 10p.<br>
                    Hệ thống thanh toán đa dạng, hỗ trợ thanh toán cho tất cả các ngân hàng tại Việt Nam.<br>
                    Hệ thống website hoạt động 24/7. Khách hàng sẽ nhận được sản phẩm sau khi chúng tôi nhận được yêu cầu trong thời gian sớm nhất. </p>
            </div>
        </div>
    </div>
    <div class="container" id='guess'>
        <div class="row">
            <div class="col-sm-6 pt-3 bg-brow">
                <h4 class="text-center text-uppercase">Trách nhiệm và nghĩa vụ của Khách hàng</h4>
                <hr>
                <p class="text-justify">Khách hàng không được phép thực hiện các hành vi lừa đảo, gian lận, giả mạo…hay các hành động truyền tải thông tin, hình ảnh xấu trái với thuần phong mỹ tục, vi phạm pháp luật Việt Nam cũng như có các hành động cố tình phá hoại, gây thiệt hại cho hệ thống website của chúng tôi. <br>
                    Các trường hợp hạn chế: Thao tác mua hàng bị hạn chế trong trường hợp trong kho hết hàng hoặc tài khoản của khách hàng không đủ số dư thực hiện giao dịch. <br>
                    Vui lòng kiểm tra tình trạng thẻ cào trước khi nạp vào hệ thống. <br>
                    Cố tình nhập sai mã thẻ nhiều lần sẽ bị KHÓA TÀI KHOẢN.</p>

            </div>
            <div class="col-sm-6 p-0">
                <img src="{{asset('in_1')}}/images/banner2.png" alt="" class="img-fluid">
            </div>
        </div>
    </div>
</div>
<footer>
    <div class="container">
        <duv class="row">
            <div class="col-sm-4">
                <img src="{{asset('in_1')}}/images/logo.png" alt="" class="img-fluid">
            </div>
            <div class="col-sm-4">
                <h5 class="text-uppercase">Liên hệ</h5>
                <hr>
                <p>
                    Hotline: 0382 645 480 <br>
                    Email : admin@congthe365.com <br>
                    Skype: 0382645480 <br>
                </p>
            </div>
            <div class="col-sm-4">
                <h5 class="text-uppercase">Chính sách</h5>
                <hr>
                <p>
                    <a href="#">Chính sách đại lý</a> <br>
                    <a href="#">Chính sách người dùng</a> <br>
                    <a href="#">Chính sách thanh toán</a>
                </p>
            </div>
        </duv>
    </div>
</footer>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>