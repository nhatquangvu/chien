<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('in_1')}}/images/icon.png">
    <title>Cổng nạp thẻ 365 - Giá trị thật, uy tin thật</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{asset('in_1/assets')}}/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{asset('in_1/assets')}}/css/style.css" rel="stylesheet">
    <link href="{{asset('in_1/assets')}}/css/custom.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="{{asset('in_1/assets')}}/css/colors/blue.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="{{asset('in_1/assets')}}/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{asset('in_1/assets')}}/plugins/bootstrap/js/popper.min.js"></script>

    <script src="{{asset('in_1/assets')}}/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{asset('in_1/assets')}}/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="{{asset('in_1/assets')}}/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="{{asset('in_1/assets')}}/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="{{asset('in_1/assets')}}/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="{{asset('in_1/assets')}}/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!--Custom JavaScript -->
    <script src="{{asset('in_1/assets')}}/js/custom.min.js"></script>
</head>

<body class="fix-sidebar fix-header card-no-border">
<!-- ============================================================== -->
<!-- Preloader - style you can find in spinners.css -->
<!-- ============================================================== -->
<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
    </svg>
</div>
<!-- ============================================================== -->
<!-- Main wrapper - style you can find in pages.scss -->
<!-- ============================================================== -->
<div id="main-wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <header class="topbar">
        <nav class="navbar top-navbar navbar-expand-md navbar-light">
            <!-- ============================================================== -->
            <!-- Logo -->
            <!-- ============================================================== -->
            <div class="navbar-header">
                <a class="navbar-brand" href="index.html">
                    <!-- Logo icon --><b>
                        <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                        <!-- Dark Logo icon -->
                        <img src="{{asset('in_1/assets')}}/images/logo-icon.png" alt="homepage" class="dark-logo"/>
                        <!-- Light Logo icon -->
                        <img src="{{asset('in_1/assets')}}/images/logo-light-icon.png" alt="homepage"
                             class="light-logo"/>
                    </b>
                    <!--End Logo icon -->
                    <!-- Logo text --><span>
                         <!-- dark Logo text -->
                         <img src="{{asset('in_1/assets')}}/images/logo-text.png" alt="homepage" class="dark-logo"/>
                        <!-- Light Logo text -->
                         <img src="{{asset('in_1/assets')}}/images/logo-light-text.png" class="light-logo"
                              alt="homepage"/></span> </a>
            </div>
            <!-- ============================================================== -->
            <!-- End Logo -->
            <!-- ============================================================== -->
            <div class="navbar-collapse">
                <!-- ============================================================== -->
                <!-- toggle and nav items -->
                <!-- ============================================================== -->
                <ul class="navbar-nav mr-auto mt-md-0">
                    <!-- This is  -->
                    <li class="nav-item"><a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark"
                                            href="javascript:void(0)"><i class="mdi mdi-menu"></i></a></li>
                    <li class="nav-item m-l-10"><a
                                class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark"
                                href="javascript:void(0)"><i class="ti-menu"></i></a></li>
                </ul>
                <!-- ============================================================== -->
                <!-- User profile and search -->
                <!-- ============================================================== -->
                <ul class="navbar-nav my-lg-0">
                    <!-- ============================================================== -->
                    <!-- Search -->
                    <!-- ============================================================== -->
                    <li class="nav-item hidden-sm-down search-box"><a
                                class="nav-link hidden-sm-down text-muted waves-effect waves-dark"
                                href="javascript:void(0)"><i class="ti-search"></i></a>
                        <form class="app-search">
                            <input type="text" class="form-control" placeholder="Search & enter"> <a class="srh-btn"><i
                                        class="ti-close"></i></a></form>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- ============================================================== -->
    <!-- End Topbar header -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    <aside class="left-sidebar">
        <!-- Sidebar scroll-->
        <div class="scroll-sidebar">
            <!-- User profile -->
            <div class="user-profile">
                <!-- User profile image -->
                <div class="profile-img"><img src="{{asset('in_1/assets')}}/images/users/profile.png" alt="user"/>
                    <!-- this is blinking heartbit-->
                    <div class="notify setpos"><span class="heartbit"></span> <span class="point"></span></div>
                </div>
                <!-- User profile text-->
                <div class="profile-text">
                    <h5>{{\Illuminate\Support\Facades\Auth::user()->name}}</h5>
                    <span class="badge badge-danger">Số dư: {{number_format(\Illuminate\Support\Facades\Auth::user()->balance)}}</span>
                    <br>

                    <a href="{{route('in::logout')}}" class="" data-toggle="tooltip" title="Thoát tài khoản"><i
                                class="mdi mdi-power"></i></a>

                </div>
            </div>
            <!-- End User profile text-->
            <!-- Sidebar navigation-->
            <nav class="sidebar-nav">
                <ul id="sidebarnav">
                    <li class="nav-devider"></li>
                    <li class="nav-small-cap text-uppercase">Nạp thẻ</li>

                    <li><a class="waves-effect waves-dark" href="{{route('in::dashboard')}}" aria-expanded="false"><i
                                    class="mdi mdi-apple-mobileme"></i><span class="hide-menu">Thẻ cào</span></a>

                    </li>
                    <li><a class="waves-effect waves-dark" href="{{route('in::bank')}}" aria-expanded="false"><i
                                    class="mdi mdi-bank"></i><span class="hide-menu">Tài khoản ngân hàng</span></a>

                    </li>

                    <li><a class=" waves-effect waves-dark" href="{{route('in::pay')}}" aria-expanded="false"><i
                                    class="mdi mdi-cards"></i>Yêu cầu rút tiền</a>

                    </li>
                    <li><a class=" waves-effect waves-dark" href="{{route('in::user')}}" aria-expanded="false"><i
                                    class="mdi mdi-code-array"></i>Đổi mật khẩu</a>

                    </li>



                </ul>
            </nav>
            <!-- End Sidebar navigation -->
        </div>
        <!-- End Sidebar scroll-->
    </aside>
    <!-- ============================================================== -->
    <!-- End Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Page wrapper  -->
    <!-- ============================================================== -->
    <div class="page-wrapper">

        @yield('content')

        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <footer class="footer">
            © 2018
        </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Page wrapper  -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
<script>
    document.getElementById('btn_submit')
        .addEventListner('click', function (event) {
            document.getElementById('add_card').submit();
        })
    ;
</script>

</body>

</html>
