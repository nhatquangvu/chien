@extends('shop.admin.template')
@section('content')
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Rút tiền về</h3>
        </div>


    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12 col-sm-8 offset-sm-2">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title text-uppercase">Yêu cầu rút tiền</h4>
                        <hr>

                            <p class="bg-warning p-2 text-white"><i class="fa fa-bell-o"></i> Quý khách hàng vui lòng kiểm tra thông tin tài khoản ngân hàng trước khi đặt lệnh rút</p>
                            <form action="{{route('shop::admin::admin_pay')}}" method="post">
                                <div class="form-group">
                                    <label for="">Nhập số tiền cần rút</label>
                                    <input type="number" name="balance" class="form-control" placeholder="Số tiền"
                                           min="50000" max="{{\Illuminate\Support\Facades\Auth::user()->balance}}" required>
                                </div>
                                <button class="btn btn-default btn-info"><i class="fa fa-money"></i> Yêu cầu rút tiền
                                </button>
                                {{csrf_field()}}
                            </form>


                    </div>
                </div>
                <div class="card mt-2">
                    <div class="card-body">
                        <h4 class="card-title text-uppercase mt-4"><i class="fa fa-clock-o"></i> Lịch sử rút tiền</h4>
                        <div class="table-responsive" id="manager_card">
                            <table id="demo-foo-addrow" class="table m-t-30 table-hover no-wrap contact-list"
                                   data-page-size="10">
                                <thead>
                                <tr>
                                    <th>ID #</th>
                                    <th>Số tiền</th>
                                    <th>Thời gian</th>
                                    <th>Trạng thái</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($pays as $p)
                                    @if($p->status == 0)
                                        <tr class="table-danger">
                                    @else
                                        <tr>
                                            @endif
                                            <td>{{$p->id}}</td>
                                            <td>{{$p->balance}}</td>
                                            <td>{{$p->created_at->format('h:i d/m/Y')}}</td>
                                            <td>
                                                @if($p->status == 1)
                                                    <span class="badge badge-success">Thành công</span>
                                                @else
                                                    <span class="badge badge-warning">Đang chờ</span>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                </tbody>
                                <tfoot>

                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->

    </div>

@endsection