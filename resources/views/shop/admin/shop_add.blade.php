@extends('shop.admin.template')
@section('content')
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Quản lý acc</h3>
        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->

        <div class="row">
            <div class="col-12 col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title text-uppercase">Thêm ADD</h4>
                        <hr>
                        <form action="{{route('shop::admin::admin_shop')}}" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="add" value="1">
                            <div class="control-group">
                                <label for="">Thêm acc</label>
                                <input type="text" name="name" placeholder="Giới thiệu ngắn" class="form-control"
                                       required>
                            </div>
                            <div class="control-group">
                                <label for="">Giá</label>
                                <input type="number" name="price" placeholder="Giá" class="form-control" required>
                            </div>

                            <div class="control-group">
                                <label for="">Tài khoản</label>
                                <input type="text" name="account" placeholder="Tên tài khoản" class="form-control"
                                       required>
                            </div>
                            <div class="control-group">
                                <label for="">Mật khẩu</label>
                                <input type="text" name="password" placeholder="Mật khẩu" class="form-control" required>
                            </div>
                            <hr>

                            <div class="control-group">
                                <label for="">Hình ảnh</label>
                                <input type="file" name="image" placeholder="Hình ảnh" class="form-control" required>
                            </div>
                            <div class="control-group">
                                <label for="">Server</label>
                                <select name="server" id="" class="form-control">
                                    <option value="Vũ trụ 1 sao" selected>Vũ trụ 1 sao</option>
                                    <option value="Vũ trụ 2 sao">Vũ trụ 2 sao</option>
                                    <option value="Vũ trụ 3 sao">Vũ trụ 3 sao</option>
                                    <option value="Vũ trụ 4 sao">Vũ trụ 4 sao</option>
                                    <option value="Vũ trụ 5 sao">Vũ trụ 5 sao</option>
                                    <option value="Vũ trụ 6 sao">Vũ trụ 6 sao</option>
                                    <option value="Vũ trụ 7 sao">Vũ trụ 7 sao</option>
                                </select>
                            </div>
                            <label for="">Chọn hành tinh</label>
                            <select name="planet" id="" class="form-control" required>
                                    <option value="Xaya">Xaya</option>
                                    <option value="Namec">Namec</option>
                                    <option value="Trái đất">Trái đất</option>
                            </select>
                            <label for="">Kiểu đăng ký</label>
                            <select name="hasGmail" id="" class="form-control">
                                <option value="0" selected>Tài khoản ảo</option>
                                <option value="1">Full Gmail</option>
                            </select>
                            <label for="">Sơ Sinh có đệ</label>
                            <select name="hasPract" id="" class="form-control">
                                <option value="0" selected>Không</option>
                                <option value="1">Có đệ</option>
                            </select>
                            <label for="">Bông tai</label>
                            <select name="hasRing" id="" class="form-control">
                                <option value="0" selected>Không</option>
                                <option value="1">Có Bông tai</option>
                            </select>
                            <label for="">Nick Sơ Sinh</label>
                            <select name="isNewBorn" id="" class="form-control">
                                <option value="0" selected>Không</option>
                                <option value="1">Nick sơ sinh</option>
                            </select>

                            <div class="control-group">
                                <label for="">Thông tin tài khoản</label>
                                <textarea id="my-editor" name="content" class="form-control"></textarea>
                            </div>
                            {{csrf_field()}}
                            <button type="submit" class="btn btn-danger mt-2"><i class="fa fa-upload"></i> Thêm tài khoản</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->

    </div>

@endsection
