@extends('shop.admin.template')
@section('content')
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Quản lý acc</h3>
        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12 col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title text-uppercase">Quản lý acc</h4>
                        <hr>
                        <a href="{{route('shop::admin::admin_shop',['add'=>1])}}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Thêm acc</a>
                        <div class="table-responsive mt-2" id="manager_card">
                            <table id="demo-foo-addrow" class="table m-t-30 table-hover no-wrap contact-list"
                                   data-page-size="10">
                                <thead>
                                <tr>
                                    <th>#ID</th>
                                    <th>Acc</th>
                                    <th>Server</th>
                                    <th>Giá</th>
                                    <th>Mua bởi</th>
                                    <th>Hành đồng</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($account as $acc)
                                    
                                    <tr>
                                        <td>{{$acc->id}}</td>
                                        <td>{{$acc->account}}</td>
                                        <td>{{$acc->server}}</td>

                                        @if($acc->user_id == 0)
                                            <td>
                                                <form action="{{route('shop::admin::admin_shop')}}" method="post" enctype="multipart/form-data">
                                                    {{csrf_field()}}
                                                    <input type="hidden" name="acc_id" value="{{$acc->id}}">
                                                    <div class="input-group mb-3">
                                                        <input type="text" class="form-control" name="price" value="{{$acc->price}}" placeholder="{{$acc->price}}" aria-label="{{$acc->price}}" aria-describedby="basic-addon2">
                                                        <div class="input-group-append">
                                                            <button class="btn btn-outline-secondary" type="submit">Sửa</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </td>
                                            <td><span class="badge badge-success">Chưa bán</span></td>
                                            <td>
                                                <a href="{{route('shop::admin::admin_shop',['del' => $acc->id])}}"
                                                   class="badge badge-danger text-white btn-confirm"><i
                                                            class="fa fa-trash"></i> Xóa</a>
                                            </td>
                                        @else
                                            <td>{{$acc->price}}</td>
                                            <td><span class="badge badge-danger">{{$acc->member->name}}</span></td>
                                            <td><span class="badge badge-danger">Đã bán</span></td>
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>

                                </tfoot>
                            </table>
                        </div>
                         {!! $account->links('pagination::bootstrap-4') !!}


                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->


    </div>


@endsection
