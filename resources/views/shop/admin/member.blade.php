@extends('shop.admin.template')
@section('content')
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Quản lý khách hàng mua ngọc</h3>
        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12 col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title text-uppercase">Quản lý thành viên</h4>
                        <hr>
                        <div class="row">
                            <div class="col-12">
                                <form action="{{route('shop::admin::admin_member')}}" method="GET" class="float-right">
                                    <input type="text" name="search" required>
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Tìm kiếm</button>
                                </form>
                            </div>
                        </div>
                        <div class="table-responsive" id="manager_card">
                            <table id="demo-foo-addrow" class="table m-t-30 table-hover no-wrap contact-list"
                                   data-page-size="10">
                                <thead>
                                <tr>
                                    <th>#ID</th>
                                    <th>Tên thành viên</th>
                                    <th>Tài khoản</th>
                                    <th>Trạng thái</th>
                                    <th>Hành đồng</th>
                                    <th>Cộng tiền</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($members as $member)
                                        <tr>
                                            <td>{{$member->id}}</td>
                                            <td>{{$member->name}}</td>
                                            <td>{{number_format($member->balance)}}</td>
                                            @if($member->isBanned == 1)
                                            <td><span class="badge badge-danger">Banned</span></td>
                                            @else
                                                <td><span class="badge badge-success">Bình thường</span></td>
                                            @endif
                                            <td>
                                                <a class="badge badge-warning text-white" href="{{route('shop::admin::admin_member',['banned' => $member->id ])}}"><i class="fa fa-upload"></i> Banned / Bỏ Banned</a>
                                            </td>
                                            <td>
                                                <form action="{{route('shop::admin::admin_member')}}" method="POST">
                                                    <input type="hidden" value="{{$member->id}}" name="member_id">
                                                    <input type="number" name="add" required>
                                                    {{csrf_field()}}
                                                    <button type="submit" class="btn btn-success"><i class="fa fa-plus"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                        @endforeach
                                </tbody>
                                <tfoot>

                                </tfoot>
                            </table>
                        </div>
                        {!! $members->links('pagination::bootstrap-4') !!}
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->

    </div>

@endsection
