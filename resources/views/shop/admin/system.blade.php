@extends('shop.admin.template')
@section('content')
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Quản lý tỉ giá vàng ngọc</h3>
        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row mt-2">
            @foreach(\App\Network::all() as $net)
                <div class="col-md-6 col-lg-4 col-xlg-4">
                    <div class="card card-danger">
                        <div class="box text-uppercase text-center text-white">
                            <h6 class="text-white">Mức chiết khấu</h6>
                            @if(\Illuminate\Support\Facades\Auth::user()->isVip == 0)
                                <h1 class="font-light font-weight-bold text-white">{{$net->rate_in_guess}}
                                    %</h1>
                            @else
                                <h1 class="font-light font-weight-bold text-white">{{$net->rate_in}}
                                    %</h1>
                            @endif

                            <h4 class="text-white font-weight-bold">{{$net->name}}</h4>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="row mt-2">
            <div class="col-12 col-sm-6 offset-sm-3">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title text-uppercase">Quản lý tỉ giá vàng ngọc</h4>
                        <hr>
                        @php
                            $user = \App\User::find(\Illuminate\Support\Facades\Session::get('shop')->id)
                        @endphp
                        <form action="{{route('shop::admin::admin_system')}}" method="post">
                            <div class="form-group">
                                <label for="">Tỉ giá vàng</label>
                                <input type="number" name="rate_gold" class="form-control"
                                       value="{{$user->rate_gold}}">
                            </div>
                            <div class="form-group">
                                <label for="">Tỉ giá ngọc</label>
                                <input type="text" name="rate_gem" class="form-control"
                                       value="{{$user->rate_gem}}">
                            </div>
                            <hr>
                            <div class="form-group">
                                <label for="">Chữ chạy</label>
                                <textarea name="warning" id="" cols="30" rows="10" class="form-control">{{\App\Page::where('short_code','shop_1')->first()->content}}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="">Popup đầu trang</label>
                                <textarea id="my-editor" name="popup" class="form-control">{{\App\Page::where('short_code','shop_1_popup')->first()->content}}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="">Thông tin ATM</label>
                                <textarea id="my-editor2" name="atm" class="form-control">{{\App\Page::where('short_code','shop_1_atm')->first()->content}}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="">Giá trị phần thưởng</label>
                                <textarea id="my-editor3" name="top" class="form-control">{{\App\Page::where('short_code','shop_1_top')->first()->content}}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="">Hotline</label>
                                <input type="text" name="hotline" class="form-control" value="{{\App\Page::where('short_code','shop_1_hotline')->first()->content}}">
                            </div>
                            <br>
                            {{csrf_field()}}
                            <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-upload"></i> Cập
                                nhập
                            </button>
                        </form>


                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->

    </div>

@endsection
