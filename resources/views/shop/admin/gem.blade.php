@extends('shop.admin.template')
@section('content')
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Quản lý khách hàng mua ngọc</h3>
        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12 col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title text-uppercase">Mua bán vàng ngọc</h4>
                        <hr>

                        <div class="table-responsive" id="manager_card">
                            <table id="demo-foo-addrow" class="table m-t-30 table-hover no-wrap contact-list"
                                   data-page-size="10">
                                <thead>
                                <tr>
                                    <th>#ID</th>
                                    <th>Loại giao dịch</th>
                                    <th>Server</th>
                                    <th>Người dùng</th>
                                    <th>Tài khoản</th>
                                    <th>Mật khẩu</th>
                                    <th>Số Ngọc</th>
                                    <th>Thời gian</th>
                                    <th>Hành động</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($sers as $ser)
                                    <tr>
                                        <td>{{$ser->id}}</td>
                                        @if($ser->isGold == 1)
                                            <td><span class="badge badge-warning">Giao dịch vàng</span></td>
                                        @else
                                            <td><span class="badge badge-success">Giao dịch ngọc</span></td>

                                        @endif
                                        <td>{{$ser->server}}</td>
                                        <td>{{$ser->member->name}}</td>
                                        <td>{{$ser->account_game}}</td>
                                        <td>{{$ser->password_game}}</td>
                                        <td>{{number_format($ser->quality)}}</td>
                                        <td>{{$ser->created_at->format('H:i d/m/Y')}}</td>
                                        <td>
                                            @if($ser->isComplete == 0)
                                                <span class="badge badge-primary" onclick="openTrade('action-{{$ser->id}}')">Giao dịch</span>
                                                @else
                                                Thành công
                                            @endif
                                        </td>
                                    </tr>
                                    @if($ser->isComplete == 0)
                                    <tr id="action-{{$ser->id}}" style="display: none">
                                        <td colspan="5">
                                                <input type="text" id="text-{{$ser->id}}" class="w-50 sendMes" data-id="{{$ser->id}}">
                                                <span class="text-danger w-50 bg-success text-white p-1" id="mess-{{$ser->id}}">{{$ser->message}}</span>
                                        </td>
                                        <td><a href="{{route('shop::admin::admin_gem',['success' => $ser->id])}}" class="badge badge-primary">Hoành thành</a>
                                            <a href="{{route('shop::admin::admin_gem',['cancel' => $ser->id])}}" class="badge badge-danger ml-2">Xóa</a></td>
                                    </tr>
                                    @endif
                                @endforeach
                                </tbody>
                                <tfoot>

                                </tfoot>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->

    </div>
    <script>
        function openTrade(id) {
            $('#' + id).toggle();
        }


        $('.sendMes').keypress(function(event){
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {
                id_ser = $(this).attr('data-id');
                mess = $(this).val();
                $.ajax({
                    url : "{{route('shop::ajax::service')}}",
                    type : "post",
                    dataType:"text",
                    data : {
                        "_token": "{{ csrf_token() }}",
                        "id": id_ser,
                        "message" : mess
                    },
                    success : function (result){
                        $('#mess-' + id_ser).html(result);
                    }
                });
                $(this).val('');
            }
        });
    </script>
@endsection
