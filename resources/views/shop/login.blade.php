@extends('shop.template')
@section('content')

    <div class="bg-wallpaper">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 offset-sm-3">
                    <div class="bg-white p-3">
                        <h5><i class="fa fa-sign-in"></i> Đăng nhập tài khoản</h5>
                        <hr>
                        @if(\Illuminate\Support\Facades\Session::has('notice'))
                            <div class="col-sm-12 col-12">
                                <div class="alert alert-danger" role="alert">
                                    {{\Illuminate\Support\Facades\Session::get('notice')}}
                                </div>
                                <br>
                            </div>
                        @endif
                        <form action="{{route('shop::login')}}" method="post">
                            <div class="form-group">
                                <label for="">Tài khoản</label>
                                <input type="text" class="form-control"
                                       aria-describedby="emailHelp" placeholder="Tài khoản" name="name">

                            </div>
                            <div class="form-group">
                                <label>Mật khẩu</label>
                                <input type="password" class="form-control"
                                       placeholder="Mật khẩu" name="password">
                            </div>
                            {{ csrf_field() }}

                            <div class="g-recaptcha" data-sitekey="6Ldaf30UAAAAAHBuuaLhBbOHi5bT2fQC4M1qpbq4"></div>
                            <br>
                            <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-sign-in"></i> Đăng
                                nhập
                            </button>
                            <br>
                            <a href="https://congthe365.com/login/facebook" class="btn facebook btn-block"><i class="fa fa-facebook"></i> Đăng
                                nhập Facebook
                            </a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection