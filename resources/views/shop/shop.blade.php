@extends('shop.template')
@section('content')

    <div class="bg-wallpaper">
        <div class="container">
            <div class="row">
                <form action="">
                    <div class="col-sm-12 mb-3 bg-white p-3" id="shop-search">
                        <h6 class="text-white"><i class="fa fa-search"></i> Tìm kiếm</h6>
                        <div class="row">
                            <div class="col-sm-3 mb-2">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Mã số</div>
                                    </div>
                                    <input type="text" class="form-control" name="numb">
                                </div>
                            </div>
                            <div class="col-sm-3 mb-2">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Mức giá</div>
                                    </div>
                                    <select name="price" id="" class="form-control">
                                        <option value="0,1000000000">Không giới hạn</option>
                                        <option value="0,100000">Từ 0 - 100K</option>
                                        <option value="100000,300000">Từ 100k - 300K</option>
                                        <option value="300000,500000">Từ 300k - 500K</option>
                                        <option value="500000,1000000">Từ 500K - 1Tr</option>
                                        <option value="1000000,5000000">Từ 1tr - 5Tr</option>
                                        <option value="5000000,50000000">Trên 5Tr</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3 mb-2">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Server</div>
                                    </div>
                                    <select name="server" id="" class="form-control">
                                        <option value="0">Tất cả</option>
                                        <option value="Vũ trụ 1 sao" selected>Vũ trụ 1 sao</option>
                                        <option value="Vũ trụ 2 sao">Vũ trụ 2 sao</option>
                                        <option value="Vũ trụ 3 sao">Vũ trụ 3 sao</option>
                                        <option value="Vũ trụ 4 sao">Vũ trụ 4 sao</option>
                                        <option value="Vũ trụ 5 sao">Vũ trụ 5 sao</option>
                                        <option value="Vũ trụ 6 sao">Vũ trụ 6 sao</option>
                                        <option value="Vũ trụ 7 sao">Vũ trụ 7 sao</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3 mb-2">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Hành tinh</div>
                                    </div>
                                    <select name="planet" id="" class="form-control">
                                        <option value="0">Tất cả</option>
                                        <option value="Xaya">Xaya</option>
                                        <option value="Namec">Namec</option>
                                        <option value="trái đất">Trái đất</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3 mb-2">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Đăng ký</div>
                                    </div>
                                    <select name="hasGmail" id="" class="form-control">
                                        <option value="0">Tất cả</option>
                                        <option value="1">Full Gmail</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3 mb-2">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Sơ sinh có đệ</div>
                                    </div>
                                    <select name="hasPract" id="" class="form-control">
                                        <option value="0">Tất cả</option>
                                        <option value="1">Có</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3 mb-2">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Bông tai</div>
                                    </div>
                                    <select name="hasRing" id="" class="form-control">
                                        <option value="0">Tất cả</option>
                                        <option value="1">Có</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3 mb-2">
                                <button class="btn btn-block btn-warning"><i class="fa fa-search"></i> LỌC</button>
                            </div>

                        </div>
                    </div>
                </form>
            </div>
            <div class="row">
                @if(\Illuminate\Support\Facades\Session::has('notice'))
                    <div class="col-sm-12 col-12">
                        <div class="alert alert-danger" role="alert">
                            {{\Illuminate\Support\Facades\Session::get('notice')}}
                        </div>
                        <br>
                    </div>
                @endif
                @if(!isset($acc_detail))
                    @foreach($account as $acc)
                        <div class="col-sm-3 col-12">
                            <div class="acc bg-white p-2 mb-2">
                                <img src="{{asset('shop/account')}}/{{$acc->image}}" alt="" class="img-fluid">
                                <span><span class="badge badge-danger text-white">Mã: {{$acc->id}}</span>{!! $acc->name !!}</span> <br>
                                <span class="font-weight-bold">Sever:</span> <span
                                        class="text-warning">{{$acc->server}}</span><br>
                                <span class="font-weight-bold">Hành tinh:</span> <span
                                        class="text-warning">{{$acc->planet}}</span><br>
                                <span class="font-weight-bold">Giá: </span> <span class="text-warning">{{number_format($acc->price)}}
                                    đ</span>
                                <br>
                                @if(!\Illuminate\Support\Facades\Session::has('login_user'))
                                    <a href="{{route('shop::login')}}">
                                        <div class="login text-white text-uppercase hvr-sweep-to-right">
                                            <i class="fa fa-sign-in"></i>
                                            Đăng nhập
                                        </div>
                                    </a>
                                    <a href="{{route('shop::register')}}">
                                        <div class="register text-white text-uppercase hvr-sweep-to-right">
                                            <i class="fa fa-user"></i>
                                            Đăng ký
                                        </div>
                                    </a>
                                @else
                                    <br>
                                    <a href="{{route('shop::acc',['detail' => $acc->id])}}">
                                        <div class="btn btn-xs btn-info text-white text-uppercase hvr-sweep-to-right">
                                            <i class="fa fa-sign-in"></i>
                                            Chi tiết
                                        </div>
                                    </a>
                                    <a href="{{route('shop::acc',['buy' => $acc->id])}}">
                                        <div class="btn btn-xs btn-danger text-white text-uppercase hvr-sweep-to-right">
                                            <i class="fa fa-user"></i>
                                            Mua
                                        </div>
                                    </a>
                                @endif
                            </div>
                        </div>
                    @endforeach
                    <div class="col-sm-12 text-center">
                        {{$account->links("pagination::bootstrap-4")}}
                    </div>
                @else
                    <div class="col-sm-12">
                        <div class="bg-white p-2">
                            <div class="col-sm-4 offset-sm-4">
                                <a href="{{route('shop::acc')}}" class="badge badge-danger"><i
                                            class="fa fa-arrow-left"></i> Quay lại shop</a>
                                <h6 class="text-center">{{$acc_detail->name}}</h6>
                                <h6>Server: {{$acc_detail->server}}</h6>
                                <h6>Hành tinh: {{$acc_detail->planet}}</h6>
                                <h6>Giá: {{number_format($acc_detail->price)}}</h6>
                            </div>
                            <div class="col-sm-12 text-center text-content">
                                <hr>
                                {!! $acc_detail->content !!}
                            </div>
                        </div>
                    </div>
                @endif
            </div>
            @if(\Illuminate\Support\Facades\Session::has('login_user'))
            <div class="row mt-3">
                <div class="col-sm-12">
                    <div class="bg-white pt-3">
                        <h5 class="m-3"><i class="fa fa-history"></i> Lịch sử giao dịch</h5>
                        <table class="table table-dark table-striped">
                            <thead>
                            <tr>
                                <th>#ID</th>
                                <th>Server</th>
                                <th>Tài khoản</th>
                                <th>Mật khẩu</th>
                                <th>Vào lúc</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($myacc as $acc)
                                <tr>
                                    <td>{{$acc->id}}</td>
                                    <td>{{$acc->server}}</td>
                                    <td>{{$acc->account}}</td>
                                    <td>{{$acc->password}}</td>
                                    <td>{{$acc->updated_at->format('H:i d/m/Y')}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <br>
                        <p class="text-center">{!! $myacc->links("pagination::bootstrap-4") !!}</p> 
                        <br>
                    </div>
                </div>
            </div>
            @endif

        </div>
    </div>


@endsection