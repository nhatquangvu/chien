@extends('shop.template')
@section('content')
    <div id="slider">
        <div id="carouselId" class="carousel slide" data-ride="carousel">

            <div class="carousel-inner" role="listbox">
                <div class="carousel-item">
                    <img src="{{asset('shop')}}/images/banner1.jpg" alt="First slide" class="img-fluid">
                </div>
                <div class="carousel-item active">
                    <img src="{{asset('shop')}}/images/banner2.jpg" alt="Second slide" class="img-fluid">
                </div>
                <div class="carousel-item">
                    <img src="{{asset('shop')}}/images/banner3.jpg" alt="Third slide" class="img-fluid">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselId" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselId" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>

    </div>
    <div id="home-icon">
        <div class="container">
            <div class="row">
                <div class="col-sm-3 col-6 mb-2">
                    <a href="{{route('shop::gold')}}"><img src="{{asset('shop/images/icon1.png')}}" alt="" class="icon img-fluid hvr-float-shadow"></a>
                </div>
                <div class="col-sm-3 col-6 mb-2">
                    <a href="{{route('shop::gem')}}"><img src="{{asset('shop/images/icon2.png')}}" alt="" class="icon img-fluid hvr-float-shadow"></a>
                </div>
                <div class="col-sm-3 col-6">
                    <a href="{{route('shop::acc')}}"><img src="{{asset('shop/images/icon3.png')}}" alt="" class="icon img-fluid hvr-float-shadow"></a>
                </div>
                <div class="col-sm-3 col-6">
                    <a href="{{route('shop::card')}}"><img src="{{asset('shop/images/icon4.png')}}" alt="" class="icon img-fluid hvr-float-shadow"></a>
                </div>
            </div>
        </div>
    </div>
    <div class="top-card">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="bg-top">
                        <h5 class="text-white text-center"><i class="fa fa-trophy"></i> TOP NẠP THẺ</h5>
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th># Thứ tự</th>
                                <th><i class="fa fa-user"></i> Tài khoản</th>
                                <th class="text-right"><i class="fa fa-dollar"></i> Số tiền</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php
                                    $f = 1;
                                    @endphp
                            @foreach(\App\Member::orderBy('thisM','desc')->take(10)->get() as $member)
                            <tr>
                                <td><i class="fa fa-trophy"></i> {{$f}}</td>
                                <td class="font-weight-bold">
                                <img src="{{$member->fbID <> ''? 'https://graph.facebook.com/v2.8/'.$member->fbID.'/picture?type=normal': asset('shop/images/icon2.png')}}" alt="" style="width: 30px; border-radius: 50%; border: 2px solid rgb(249, 168, 40)">
                                
                                {{$member->name}}</td>
                                <td class="text-right"><span class="btn btn-sm btn-danger"><i class="fa fa-dollar"></i> {{number_format($member->thisM)}}</span></td>
                            </tr>
                            @php
                                $f += 1;
                            @endphp
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
                <div class="col-md-6">
                    @php
                        $topPrev = \App\Member::orderBy('prevM','desc')->take(4)->get();
                    @endphp
                    <div class="bg-top2 pb-3">
                        <h5 class="text-white text-center"><i class="fa fa-trophy"></i> DANH SÁCH TRÚNG THƯỞNG THÁNG TRƯỚC</h5>
                        <div class="p-3">
                            <p class="text-justify">Xin chúc mừng các thành viên trong top  thành viên nạp thẻ nhiều nhất tháng đã đạt giải thưởng THANH VIÊN TÍCH CỰC</p>
                            <p><i class="fa fa-trophy"></i> TOP 1 : <span class="btn btn-success">{{$topPrev[0]->name}}</span></p>
                            <p><i class="fa fa-trophy"></i> TOP 2 : <span class="btn btn-success">{{$topPrev[1]->name}}</span></p>
                            <p><i class="fa fa-trophy"></i> TOP 3 : <span class="btn btn-success">{{$topPrev[2]->name}}</span></p>
                            <p><i class="fa fa-trophy"></i> KHUYẾN KHÍCH : <span class="btn btn-success">{{$topPrev[3]->name}}</span></p>
                            <p class="text-justify">{!! \App\Page::where('short_code','shop_1_top')->first()->content !!}</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <br>
    <!-- The Modal -->
    <div class="modal" id="myModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title"><i class="fa fa-bell"></i> Loa loa ! Thông báo</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    {!! \App\Page::where('short_code','shop_1_popup')->first()->content  !!}
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Đóng</button>
                </div>

            </div>
        </div>
    </div>
    @endsection