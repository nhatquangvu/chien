@extends('shop.template')
@section('content')
    @if(\Illuminate\Support\Facades\Session::has('login_user'))
        @php
            $member = \App\Member::find(\Illuminate\Support\Facades\Session::get('login_user')->id);
        @endphp
    <div class="bg-wallpaper">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="bg-white p-3">
                        <div class="row">
                            <div class="col-sm-6">
                                <h5><i class="fa fa-sign-in"></i> Mua ngọc</h5>
                                <hr>
                                @if(\Illuminate\Support\Facades\Session::has('notice'))
                                    <div class="col-sm-12 col-12">
                                        <div class="alert alert-danger" role="alert">
                                            {{\Illuminate\Support\Facades\Session::get('notice')}}
                                        </div>
                                        <br>
                                    </div>
                                @endif
                                <form action="{{route('shop::gem')}}" method="post">
                                    <div class="form-group">
                                        <label for="" class="text-uppercase">Chọn server</label>
                                        <select name="server" id="" class="form-control">
                                            <option value="Vũ trụ 1 sao" selected>Vũ trụ 1 sao</option>
                                            <option value="Vũ trụ 2 sao">Vũ trụ 2 sao</option>
                                            <option value="Vũ trụ 3 sao">Vũ trụ 3 sao</option>
                                            <option value="Vũ trụ 4 sao">Vũ trụ 4 sao</option>
                                            <option value="Vũ trụ 5 sao">Vũ trụ 5 sao</option>
                                            <option value="Vũ trụ 6 sao">Vũ trụ 6 sao</option>
                                            <option value="Vũ trụ 7 sao">Vũ trụ 7 sao</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control"
                                               placeholder="Tài khoản" name="account_game" required>
                                    </div>                                    <div class="form-group">
                                        <input type="password" class="form-control"
                                               placeholder="Mật khẩu" name="password_game" required>
                                    </div>
                                    <div class="form-group">
                                        @if($member->balance < 50000)
                                            <span class="btn btn-xs btn-danger p-2"><i class="fa fa-bell-o"></i> Giao dịch ngọc bạn cần tối thiểu 50.000</span>
                                            @else
                                            <select name="balance" id="buy" class="form-control">
                                                @foreach(\App\Price::where('price','>=',50000)->where('price','<=',$member->balance)->get() as $price)
                                                    <option value="{{$price->price}}">{{number_format($price->price)}}</option>
                                                @endforeach
                                                @if($member->balance >= 2000000)
                                                        <option value="2000000">{{number_format(2000000)}}</option>
                                                    @endif
                                                @if($member->balance >= 5000000)
                                                        <option value="5000000">{{number_format(5000000)}}</option>
                                                    @endif
                                                @if($member->balance >= 10000000)
                                                        <option value="10000000">{{number_format(10000000)}}</option>
                                                    @endif

                                            </select>
                                            @endif


                                    </div>
                                    <p>
                                        Hệ số: <span class="badge badge-info">x{{\Illuminate\Support\Facades\Session::get('shop')->rate_gem}}</span>
                                        <br>
                                        Số ngọc: <span  class="badge badge-info number" id="ratex"></span>
                                    </p>
                                    <script>
                                        $('#buy').change(function () {
                                            var result = $('#buy').val() * {{\Illuminate\Support\Facades\Session::get('shop')->rate_gem}}
                                            $('#ratex').html(result);
                                            $('#quality').val(result);
                                            $('#ratex').number( true );
                                        });
                                    </script>
                                    {{ csrf_field() }}
                                        <div class="g-recaptcha" data-sitekey="6Ldaf30UAAAAAHBuuaLhBbOHi5bT2fQC4M1qpbq4"></div>
                                        <br>
                                        @if($member->balance >= 50000)
                                        <button type="submit" class="btn btn-info btn-block"><i class="fa fa-dollar"></i>
                                        Mua Ngọc
                                    </button>
                                    @endif
                                </form>
                            </div>
                            <div class="col-sm-6 bl-1">
                                <h5>Hướng dẫn giao dịch</h5>
                                <hr>
                                Hệ thống bán ngọc xanh Tự Động 100%, Mật khẩu được bảo mật an toàn tuyệt đối, không sợ mất nick (Nếu bạn chưa biết cách giao dịch hãy xem hướng dẫn mua ngọc)

                                <br>  Giá ngọc x{{\Illuminate\Support\Facades\Session::get('shop')->rate_gem * 1000}} , tức 100k kí gửi {{100 * \Illuminate\Support\Facades\Session::get('shop')->rate_gem * 1000}} Bán từ 50k trở lên
                                <br>  Khi mua ngọc hãy tải lại trang xem phần Lịch Sử Mua Ngọc theo dõi Trạng Thái để biết tiến trình giao dịch
                                <br>  Hệ thống báo Giao dịch thất bại (Không tìm thấy nhân vật, Nhận item lỗi) các bạn thực hiện lại giao dịch là được nhé.
                                <br>  Yều cầu trước khi mua ngọc:
                                <br>  1. Nhân vật phải đến sẵn Siêu Thị và thoát ra trước khi mua ngọc tại đây
                                <br>  2. Có ít nhất 1 ngọc để kí gửi.
                                <br>  3. Hành trang phải có ít nhất 1 chỗ trống.
                                <br>  4. Không cài mật khẩu rương.
                                <br>  Nếu không thỏa mãn 4 yêu cầu trên thì không thể nhận ngọc và hệ thống sẽ tự động hoàn lại tiền vào tài khoản

                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-sm-12">
                                <h5><i class="fa fa-history"></i> Lịch sử giao dịch</h5>
                                <table class="table table-dark table-striped">
                                    <thead>
                                    <tr>
                                        <th>#ID</th>
                                        <th>Server</th>
                                        <th>Nhân Vật</th>
                                        <th>Số Ngọc</th>
                                        <th>Thời gian</th>
                                        <th>Trạng thái</th>
                                        <th style="width: 300px">Tin nhắn</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($sers as $ser)
                                        <tr>
                                            <td>{{$ser->id}}</td>
                                            <td>{{$ser->server}}</td>
                                            <td>{{$ser->account_game}}</td>
                                            <td>{{number_format($ser->quality)}}</td>
                                            <td>{{$ser->created_at->format('H:i d/m/Y')}}</td>
                                        @if($ser->isComplete == 1 )
                                                <td><span class="badge badge-success">Thành công</span></td>
                                                <td>Giao dịch thành công</td>
                                            @else
                                                <td><span class="badge badge-primary"><i class="fa fa-clock-o"></i> Đang chờ</span></td>
                                                <td><span id="mes-{{$ser->id}}" class="bg-danger p-1">{{$ser->message}}</span></td>
                                            @endif
                                        </tr>
                                        @if($ser->isComplete == 0)
                                        <script>
                                            setInterval(function () {
                                                $.ajax({
                                                    url : "{{route('shop::ajax::service')}}",
                                                    type : "post",
                                                    dataType:"text",
                                                    data : {
                                                        "_token": "{{ csrf_token() }}",
                                                        "id": {{$ser->id}}
                                                    },
                                                    success : function (result){
                                                        $('#mes-{{$ser->id}}').html(result);
                                                    }
                                                });

                                            },5000)
                                        </script>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @else
        <div class="bg-wallpaper">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 offset-sm-3 bg-white p-3">
                        <h5>Hướng dẫn mua ngọc</h5>
                        <hr>
                        Sau khi đặt đơn hàng, bạn hãy thoát nick ra và xem tình trạng đơn hàng bên dưới <br>
                        Thành công tức có nghĩa là giao dịch đã thành công và bạn có thể vào nick <br>
                        Lỗi tức có nghĩa là tk mk sai hoặc nick ko có 1 ngọc để kí, hoặc chưa qua được siêu thị <br>
                        Hãy đến sẵn Siêu thị và thoát nick ra, Chờ Admin vào nick chuyển ngọc là xong

                        <a href="{{route('shop::login')}}">
                            <div class="login text-white text-uppercase hvr-sweep-to-right">
                                <i class="fa fa-sign-in"></i>
                                Đăng nhập
                            </div>
                        </a>
                        <a href="{{route('shop::register')}}">
                            <div class="register text-white text-uppercase hvr-sweep-to-right">
                                <i class="fa fa-user"></i>
                                Đăng ký
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection