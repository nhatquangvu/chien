@php
    if(!\Illuminate\Support\Facades\Session::has('user_id')){
        \Illuminate\Support\Facades\Session::put('user_id',env('SHOP_1'));
    }
    $shop = \App\User::find(env('SHOP_1'));
    \Illuminate\Support\Facades\Session::put('shop',$shop);
@endphp
        <!doctype html>
<html lang="en">
<head>
    <title>Vangngoc24h.vn - Shop mua bán vàng ngọc, nick ngọc rồng uy tín chất lượng hàng đầu Việt Nam</title>
    <link rel="icon" href="{{asset('shop/images/icon2.png')}}">
    
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css"
          integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('shop')}}/css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/hover.css/2.3.1/css/hover-min.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
            integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js"
            integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/df-number-format/2.1.6/jquery.number.min.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css" />

</head>
<body>
<div id="top" class="bg2 text-white font-weight-bold">
    <div class="container">
        <div class="row">
            <div class="col-6">
                <i class="fa fa-phone-square"></i> {{\App\Page::where('short_code','shop_1_hotline')->first()->content}}
            </div>
            <div class="col-6 text-right">
                <a href="https://www.facebook.com/chien.anh.71404"><i class="fa fa-facebook-official mr-2" aria-hidden="true"></i></a>
                <a href="https://www.youtube.com/c/ss4gaming"><i class="fa fa-youtube mr-2" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
</div>
<div id="top-nav" class="bg1 text-white">
    <div class="container">
        <div class="row">
            <div class="col-sm-10 col-12">
                <a href="{{route('shop::index')}}"><img src="{{asset('shop')}}/images/logo.png" alt="" id="logo" class="img-fluid"></a>
                <ul class="d-none d-sm-block">
                    <li><a href="{{route('shop::index')}}"><i class="fa fa-home"></i> Trang chủ</a></li>
                    <li><a href="{{route('shop::gold')}}"><i class="fa fa-usd"></i> Mua vàng</a></li>
                    <li><a href="{{route('shop::gem')}}"><i class="fa fa-diamond"></i> Mua ngọc</a></li>
                    <li><a href="{{route('shop::acc')}}"><i class="fa fa-user"></i> Acc Ngọc Rồng</a></li>
                    <li><a href="{{route('shop::card')}}"><i class="fa fa-credit-card-alt"></i> Nạp thẻ</a></li>
                    <li><a href="{{route('shop::tutorial')}}"><i class="fa fa-credit-card-alt"></i> Nạp ATM</a></li>
                </ul>
            </div>
            <div class="col-sm-2 col-12">

                @if(!\Illuminate\Support\Facades\Session::has('login_user'))
                    <a href="{{route('shop::login')}}">
                        <div class="login text-white text-uppercase hvr-sweep-to-right">
                            <i class="fa fa-sign-in"></i>
                            Đăng nhập
                        </div>
                    </a>
                    <a href="{{route('shop::register')}}">
                        <div class="register text-white text-uppercase hvr-sweep-to-right">
                            <i class="fa fa-user"></i>
                            Đăng ký
                        </div>
                    </a>
                @else
                    @php
                        $member = App\Member::find(\Illuminate\Support\Facades\Session::get('login_user')->id);
                        \Illuminate\Support\Facades\Session::put('login_user',$member);
                    @endphp
                    <div class="member-info">
                        <p>{{$member->name}} <br>
                            <span class="font-weight-bold badge badge-success text-white text-uppercase"> Số dư : {{number_format($member->balance)}}</span>
                            <br>
                            <a href="{{route('shop::logout')}}"><span class="badge badge-danger text-white"><i
                                            class="fa fa-sign-out"></i> Thoát</span></a>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
@yield('content')
<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-6">
                <p><img src="{{asset('shop/images/logo.png')}}" class="logo-footer"></p>
                <p>Mọi thông tin bản quyền vui lòng liên hệ vangngoc24h.vn</p>
                <span class="font-weight-bold">Facebook:</span> <a href="https://www.facebook.com/chien.anh.71404">fb.com/chien.anh.71404</a> <br>
                <span class="font-weight-bold">Website:</span> vangngoc24h.vn <br>
                <span class="font-weight-bold">Hotline:</span> {{\App\Page::where('short_code','shop_1_hotline')->first()->content}}</p>
            </div>
            <div class="col-12">
                <p class="text-center "><small class="text-white">© Thiết kế bởi : <a href="https://fb.com/nhatquangvu89">nhatquang.vu</a></small></p>
            </div>
        </div>
    </div>
</div>
<div id="rate">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <marquee>
                    <i class="fa fa-bell"></i> {{\App\Page::where('short_code','shop_1')->first()->content}}
                </marquee>
            </div>
        </div>
    </div>
</div>
<!-- Optional JavaScript -->


<script src="{{asset('shop')}}/js/script.js"></script>
<script>
    var forms = document.querySelectorAll('div.g-recaptcha');
    forms.forEach(element => {
        element.parentElement.addEventListener("submit", function(event) {
            if (grecaptcha.getResponse() === '') {
                event.preventDefault();
                alert('Bạn phải xác nhận capcha');
            }
        }, false)
    });
    $('#myModal').modal();
</script>
</body>
</html>