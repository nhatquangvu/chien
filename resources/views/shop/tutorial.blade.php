@extends('shop.template')
@section('content')

    <div class="bg-wallpaper">
        <div class="container">

            <div class="row mt-3">
                <div class="col-sm-8 offset-sm-2">
                    <div class="bg-white p-3">
                        <h5 class=""><i class="fa fa-history"></i> Nạp ATM</h5>
                        <hr>
                        {!! \App\Page::where('short_code','shop_1_atm')->first()->content  !!}
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection