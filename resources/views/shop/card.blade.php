@extends('shop.template')
@section('content')
    @php
        $member = \App\Member::find(\Illuminate\Support\Facades\Session::get('login_user')->id);
    @endphp
    <div class="bg-wallpaper">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="bg-white p-3">
                        <div class="row">
                            <div class="col-sm-6 offset-sm-3">
                                <h5><i class="fa fa-sign-in"></i> Nạp thẻ chậm</h5>
                                <hr>
                                @if(\Illuminate\Support\Facades\Session::has('notice'))
                                    <div class="col-sm-12 col-12">
                                        <div class="alert alert-danger" role="alert">
                                            {{\Illuminate\Support\Facades\Session::get('notice')}}
                                        </div>
                                        <br>
                                    </div>
                                @endif
                                
                                Bảo trì

                            </div>

                        </div>
                        <div class="row mt-3">
                            <div class="col-sm-12">
                                <h5><i class="fa fa-history"></i> Lịch sử giao dịch</h5>
                                <p><span class="badge badge-success">Thành công</span>: Thẻ thành công <span
                                            class="badge badge-danger">Thẻ hủy</span>: Sai mã thẻ <span
                                            class="badge badge-warning">Chờ xử lý</span>: Chờ xử lý</p>
                                <table class="table table-dark table-striped">
                                    <thead>
                                    <tr>
                                        <th>#ID</th>
                                        <th>Mạng</th>
                                        <th>Mệnh giá</th>
                                        <th>Số Serial</th>
                                        <th>Mã thẻ</th>
                                        <th>Trạng thái</th>
                                        <th>Vào lúc</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($cards as $card)
                                        <tr>
                                            <td>{{$card->id}}</td>
                                            <td>{{$card->network->name}}</td>
                                            <td>{{number_format($card->price->price)}}</td>
                                            <td>{{$card->serial}}</td>
                                            <td>{{$card->key}}</td>
                                            @if($card->isDone == 0)
                                                <td id="checking-{{$card->id}}"><span class="badge badge-warning">Chờ xử lý</span>
                                                </td>
                                                @php $check = 1 @endphp
                                            @else
                                                @if($card->trans)
                                                    @if($card->isDone == 1 && $card->trans->isUsed == 0)
                                                        <td id="checking-{{$card->id}}"><span
                                                                    class="badge badge-warning">Chờ xử lý</span></td>
                                                        @php $check = 1 @endphp

                                                    @elseif($card->isDone  == 1 && $card->trans->isUsed == 1 && $card->isCorrect == 1)
                                                        <td id="checking-{{$card->id}}"><span
                                                                    class="badge badge-success">Thành công</span></td>
                                                        @php $check = 0 @endphp

                                                    @else
                                                        <td id="checking-{{$card->id}}"><span
                                                                    class="badge badge-danger">Thẻ hủy</span></td>
                                                        @php $check = 0 @endphp

                                                    @endif
                                                @else
                                                    @php $check = 1 @endphp
                                                    <td id="checking-{{$card->id}}"><span class="badge badge-warning">Chờ xử lý</span>
                                                    </td>
                                                @endif

                                            @endif
                                            <td>{{$card->created_at->format('H:i d/m/Y')}}</td>
                                        </tr>
                                        @if($check == 1)
                                            <script>
                                                setInterval(function () {
                                                    $.ajax({
                                                        url: "{{route('shop::ajax::card')}}",
                                                        type: "post",
                                                        dataType: "text",
                                                        data: {
                                                            "_token": "{{ csrf_token() }}",
                                                            "id": {{$card->id}}
                                                        },
                                                        success: function (result) {
                                                            $('#checking-{{$card->id}}').html(result);
                                                        }
                                                    });

                                                }, 5000)
                                            </script>
                                        @endif

                                    @endforeach
                                    </tbody>
                                </table>
                                <p class="text-center">{!! $cards->links("pagination::bootstrap-4") !!} </p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection